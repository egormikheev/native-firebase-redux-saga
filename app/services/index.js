/**
 * Mocking client-server processing
 */

import firebase from 'react-native-firebase'
import Utils from '../utils'
import m from 'moment'

var config = {
  apiKey: 'AIzaSyBG_vpK4Se9SS6d0SwIXid4Na5MOO_8g14',
  authDomain: 'bigdog-7b6c2.firebaseapp.com',
  databaseURL: 'https://bigdog-7b6c2.firebaseio.com/',
  project_id: 'bigdog-7b6c2',
  storageBucket: 'bigdog-7b6c2.appspot.com'
}

// Get a reference to the database service
var database = firebase.database()
let fs = firebase.firestore()

export const api = {
  getDictionary (doc) {
    let splited = doc.split('/')
    let name = doc.includes('/') ? splited[splited.length - 1] : doc
    return new Promise((resolve, reject) => {
      let items = {}
      fs.collection(doc).get().then(snapshot => {
        let docs = snapshot.docs
        let doscarr = []
        for (let item of docs) {
          doscarr.push(item.data())
        }
        resolve(doscarr)
      })
    })
    // return firebase.database().ref('/store/gyms').once('value')
  },

  addNewTrain (train) {
    return new Promise((resolve, reject) => {
      fs
        .collection('trains')
        .add(train)
        .then(docRef => {
          resolve(docRef.id)
        })
        .catch(err => {
          Utils.log(err)
        })
    })
  },

  getTrainings (userId, withSystem) {
    return new Promise((resolve, reject) => {
      let query

      if (withSystem) {
        query = fs
          .collection('trains')
          .where('userId', '==', '0000000000000000000000000000')
        if (userId) query.where('userId', '==', userId)
      } else if (userId) {
        query = fs.collection('trains').where('userId', '==', userId)
      } else {
        resolve(null)
      }
      query
        .get()
        .then(snapshot => {
          let docs = snapshot.docs
          let trains = []
          for (let item of docs) {
            let data = item.data()
            trains.push({ id: item.id, ...data })
          }
          resolve(trains)
        })
        .catch(e => {
          Utils.log(e)
        })
    })
  },

  getPrograms (userId, withSystem) {
    return new Promise((resolve, reject) => {
      let query

      if (withSystem) {
        query = fs
          .collection('programs')
          .where('userId', '==', '0000000000000000000000000000')
        if (userId) query.where('userId', '==', userId)
      } else if (userId) {
        query = fs.collection('programs').where('userId', '==', userId)
      } else {
        resolve(null)
      }
      query
        .get()
        .then(snapshot => {
          let docs = snapshot.docs
          let programs = []
          for (let item of docs) {
            let data = item.data()
            programs.push({ id: item.id, ...data })
          }
          resolve(programs)
        })
        .catch(e => {
          Utils.log(e)
        })
    })
  },

  getUserData () {
    return firebase.database().ref('/store/actions').once('value')
  },

  getCalendarDaysActivity (date) {
    let startAt = m(date, 'YYYY-MM-DD').day(-100).format('x')
    let endAt = m(date, 'YYYY-MM-DD').day(50).format('x')
    try {
      return firebase
        .database()
        .ref('/store/actions')
        .orderByChild('dateStop')
        .startAt(+startAt)
        .endAt(+endAt)
        .once('value')
    } catch (e) {
      Utils.log('Error in service')
    }
  },

  getAllSensorData () {
    try {
      return database.ref('store').once('value')
    } catch (e) {
      Utils.log('getAllSensorData error')
    }
  },

  clearAllSensorData () {
    try {
      return database.ref('store').remove()
    } catch (e) {
      Utils.log('clearAllSensorData error')
    }
  }
}
