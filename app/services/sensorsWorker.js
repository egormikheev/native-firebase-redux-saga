import Utils from '../utils'
import l from 'lodash'
import {
  connect
} from 'react-redux'
import React, {
  Component
} from 'react'
import {
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  AppState,
  Vibration
} from 'react-native'

import m from 'moment'
import RNFS from 'react-native-fs'

import * as Buffer from 'buffer'

import BleManager from 'react-native-ble-manager'
import {
  sendAccelerometerData
} from '../redux/actions/training'
import {
  setPeripherals
} from '../redux/actions/bluetooth'

import {
  Accelerometer,
  Gyroscope,
  Magnetometer
} from 'react-native-sensors'

// import App from "../../App";
import SideBar from '../containers/sidebar'

const BleManagerModule = NativeModules.BleManager
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule)

class BleService extends Component {
  constructor(props) {
    super(props)

    this.state = {
      boundedDevice: null,
      scanning: false,
      peripherals: new Map(),
      appState: '',
      bleState: false,
      deviceSensorsState: false,
      sensors: {
        accelerometer: {
          data: new Map(),
          state: true
        },
        magnetometer: {
          data: new Map(),
          state: true
        },
        gyroscope: {
          data: new Map(),
          state: true
        }
      }
    }

    this.service = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
    this.bakeCharacteristic = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E'
    this.crustCharacteristic = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E'

    this.accelerationObservable = null
    this.gyroObservable = null
    this.magnetoObservable = null
    this.interval = null

    this.accelbuffer = []
  }

  componentWillReceiveProps(nextProps) {
    Utils.log(this.state)
    if(this.state.appState === '') {
      if (this.state.bleState == false || this.state.scanning == false) {
        this.enableBle().then(() => {
          this.getBondedGyro().then((result) => {
            Utils.log(result)
            this.startBle()
            this.testBle(result)
          })
        })
      } else {
        this.getBondedGyro().then((result) => {
          Utils.log(result)
          this.startBle()
          this.testBle(result)
        })
      }
      this.setState({ appState: AppState.currentState })
    }
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  startBle = () => {
    if (!this.state.boundedDevice) {
      Utils.log('Ble started')
      BleManager.start({
        showAlert: false
      })

      this.handlerDiscover = bleManagerEmitter.addListener(
        'BleManagerDiscoverPeripheral',
        this.handleDiscoverPeripheral
      )
      this.handlerStop = bleManagerEmitter.addListener(
        'BleManagerStopScan',
        this.handleStopScan
      )
      this.handlerDisconnect = bleManagerEmitter.addListener(
        'BleManagerDisconnectPeripheral',
        this.handleDisconnectedPeripheral
      )
      this.handlerUpdate = bleManagerEmitter.addListener(
        'BleManagerDidUpdateValueForCharacteristic',
        this.handleUpdateValueForCharacteristic
      )

      this.handleDidUpdateState = bleManagerEmitter.addListener(
        'BleManagerDidUpdateState',
        this.handleDidUpdateState
      )
    }
  }

  checkStateBle = async () => {
    return new Promise((resolve, reject) => {
      if (Platform.OS === 'android' && Platform.Version >= 23) {
        PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
        ).then(result => {
          if (result) {
            Utils.log('Permission is OK')
          } else {
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
            ).then(result => {
              if (result) {
                Utils.log('User accept')
              } else {
                Utils.log('User refuse')
              }
            })
          }
          Utils.log(BleManager.checkState())
          resolve(BleManager.checkState())
        })
      }
    })
  }

  enableDeviceSensors = duration => {
    this.setState({
      deviceSensorsState: true
    })
    new Accelerometer({
        updateInterval: duration // defaults to 100ms
      })
      .then(observable => {
        this.accelerationObservable = observable
        this.accelerationObservable.subscribe(data => {
          let accelerometerData = this.state.sensors.accelerometer.data
          accelerometerData.set(data.timestamp, data)
          this.setState({
            sensors: {
              ...this.state.sensors,
              accelerometer: {
                data: accelerometerData,
                state: true
              }
            }
          })
        })
      })
      .catch(err => {
        this.setState({
          sensors: {
            ...this.state.sensors,
            accelerometer: {
              state: false
            }
          }
        })
      })

    new Gyroscope({
        updateInterval: duration // defaults to 100ms
      })
      .then(observable => {
        this.gyroObservable = observable
        this.gyroObservable.subscribe(data => {
          let gyroscopeData = this.state.sensors.gyroscope.data
          gyroscopeData.set(data.timestamp, data)
          this.setState({
            sensors: {
              ...this.state.sensors,
              gyroscope: {
                data: gyroscopeData,
                state: true
              }
            }
          })
        })
      })
      .catch(err => {
        this.setState({
          sensors: {
            ...this.state.sensors,
            gyroscope: {
              state: false
            }
          }
        })
      })

    new Magnetometer({
        updateInterval: duration // defaults to 100ms
      })
      .then(observable => {
        this.magnetoObservable = observable
        this.magnetoObservable.subscribe(data => {
          let magnetometerData = this.state.sensors.magnetometer.data
          magnetometerData.set(data.timestamp, data)
          this.setState({
            sensors: {
              ...this.state.sensors,
              magnetometer: {
                data: magnetometerData,
                state: true
              }
            }
          })
        })
      })
      .catch(err => {
        this.setState({
          sensors: {
            ...this.state.sensors,
            magnetometer: {
              state: false
            }
          }
        })
      })
  }

  disableDeviceSensors = () => {
    if (this.state.sensors.accelerometer.state) {
      this.accelerationObservable.stop()
    }
    if (this.state.sensors.gyroscope.state) {
      this.gyroObservable.stop()
    }
    if (this.state.sensors.magnetometer.state) {
      this.magnetoObservable.stop()
    }

    this.setState({
      deviceSensorsState: false,
      sensors: {
        accelerometer: {
          data: new Map(),
          state: false
        },
        magnetometer: {
          data: new Map(),
          state: false
        },
        gyroscope: {
          data: new Map(),
          state: false
        }
      }
    })
  }

  clearDeviceSensorsData = () => {
    this.setState({
      sensors: {
        accelerometer: {
          data: new Map(),
          state: true
        },
        magnetometer: {
          data: new Map(),
          state: true
        },
        gyroscope: {
          data: new Map(),
          state: true
        }
      }
    })
  }

  getDeviceSensorsData = () => {
    let rawdata = l.cloneDeep(this.state.sensors)

    let aData, gData, mData
    if (rawdata.accelerometer.state) {
      aData = [...rawdata.accelerometer.data.values()]
    }

    if (rawdata.gyroscope.state) {
      gData = [...rawdata.gyroscope.data.values()]
    }

    if (rawdata.magnetometer.state) {
      mData = [...rawdata.magnetometer.data.values()]
    }

    this.setState({
      sensors: {
        accelerometer: {
          data: new Map(),
          state: true
        },
        magnetometer: {
          data: new Map(),
          state: true
        },
        gyroscope: {
          data: new Map(),
          state: true
        }
      }
    })

    return {
      accelerometer: aData,
      gyroscope: gData,
      magnetometer: mData
    }
  }

  enableBle = () => {
    return new Promise(resolve => {
    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
      ).then(result => {
        if (result) {
          Utils.log('Permission is OK')
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
          ).then(result => {
            if (result) {
              Utils.log('User accept')
            } else {
              Utils.log('User refuse')
            }
          })
        }
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
          .then(() => {
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE)
              .then(() => {
                if (!BleManager.checkState()) {
                  BleManager.enableBluetooth()
                    .then(() => {
                      Utils.log('The bluetooh is already enabled or the user confirm')
                      resolve(true)
                    })
                    .catch(error => {
                      Utils.log('The user refuse to enable bluetooth')
                    })
                }
              })
          })
      })
    }
  })
  }

  handleDidUpdateState = state => {
    this.setState({
      bleState: state.state
    })
  }

  handleAppStateChange = nextAppState => {
    Utils.log({ appState:  this.state.appState })
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      Utils.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then(peripheralsArray => {
        this.props.setPeripherals(peripheralsArray)
        Utils.log('Connected peripherals: ' + peripheralsArray.length)
      })
    }
    this.setState({
      appState: nextAppState
    })
  }

  stopBle = () => {
    Utils.log('Ble unmount')
    this.handlerDiscover.remove()
    this.handlerStop.remove()
    this.handlerDisconnect.remove()
    this.handlerUpdate.remove()
    this.setState({
      boundedDevice: null
    })
  }

  handleDisconnectedPeripheral = data => {
    let peripherals = this.state.peripherals
    let peripheral = peripherals.get(data.peripheral)
    if (peripheral) {
      peripheral.connected = false
      peripherals.set(peripheral.id, peripheral)
      this.props.setPeripherals(peripherals)
    }
    Utils.log('Disconnected from ' + data.peripheral)
    Vibration.vibrate([50, 500, 100, 500, 100, 700])
    this.startBle()
    const interval = setInterval(() => {
      Utils.log('try to reconnect' + data.peripheral)
      this.getBondedGyro().then((gyro) => {
        this.testBle(gyro)
      })
    }, 5000);
    this.setState({interval})
  }

  handleUpdateValueForCharacteristic = data => {
    const buffer = Buffer.Buffer.from(data.value) // https://github.com/feross/buffer#convert-arraybuffer-to-buffer
    const sensorData = buffer.toString()

    this.accelbuffer.push({
      data: sensorData,
      date: Date.now() / 1000
    })
    if (this.accelbuffer.length == 100) {
      let deviceData = false
      if (this.state.deviceSensorsState) {
        deviceData = this.getDeviceSensorsData()
      }
      const path = RNFS.ExternalStorageDirectoryPath +
    `/Documents/${m().format('YYYYMMDD')}datasensors.txt`
    const filedata = JSON.stringify({
    bracerData: this.accelbuffer,
    deviceData: deviceData
    }, null, 2)

    RNFS.appendFile(path, filedata).then(() => {
      // this.props.sendAccelerometerData({
      //   bracerData: this.accelbuffer,
      //   deviceData: deviceData
      // })
      this.accelbuffer = []
    })
    }
  }

  handleStopScan = () => {
    Utils.log('Scan is stopped')
    this.setState({
      scanning: false
    })
  }

  retrieveConnected = () => {
    BleManager.getConnectedPeripherals([]).then(results => {
      Utils.log('get-connected-device-info')
      Utils.log(results)
      var peripherals = this.state.peripherals
      for (var i = 0; i < results.length; i++) {
        var peripheral = results[i]
        peripheral.connected = true
        peripherals.set(peripheral.id, peripheral)
        this.props.setPeripherals(peripherals)
        // this.setState({ peripherals });
      }
    })
  }

  handleDiscoverPeripheral = peripheral => {
    var peripherals = this.state.peripherals
    if (!peripherals.has(peripheral.id)) {
      Utils.log({
        'Got ble peripheral': peripheral
      })
      peripherals.set(peripheral.id, peripheral)
      this.props.setPeripherals(peripherals)
      // this.setState({ peripherals });
    }
  }

  boundPeripheral = peripheral => {
    BleManager.getBondedPeripherals([]).then(bondedPeripheralsArray => {
      // Each peripheral in returned array will have id and name properties
      Utils.log('Bonded peripherals: ' + bondedPeripheralsArray.length)
      if (!bondedPeripheralsArray.includes(peripheral.id)) {
        BleManager.createBond(peripheral.id)
          .then(() => {
            Utils.log('createBond success or there is already an existing one')
          })
          .catch(() => {
            Utils.log('fail to bond')
          })
      }
    })
  }

  getBondedGyro = async () => {
    return new Promise((resolve, reject) => {
      BleManager.getBondedPeripherals([]).then(results => {
        // Each peripheral in returned array will have id and name properties
        Utils.log({
          results
        })
        const gyro = results.find(item => item.name === 'ESP32GYRO')
        if (gyro) {
          resolve(gyro)
        }
      })
    })
  }

  testBle = (peripheral) => {
    if (peripheral) {
      if (peripheral.connected) {
        BleManager.disconnect(peripheral.id)
      } else {
        BleManager.connect(peripheral.id)
          .then(() => {
            let peripherals = this.state.peripherals
            let p = peripherals.get(peripheral.id)
            if (p) {
              p.connected = true
              peripherals.set(peripheral.id, p)
              this.props.setPeripherals(peripherals)
              // this.setState({ peripherals });
            }
            Utils.log('Connected to ' + peripheral.id)

            setTimeout(() => {
              /* Test read current RSSI value
            BleManager.retrieveServices(peripheral.id).then((peripheralData) => {
              Utils.log('Retrieved peripheral services', peripheralData);
              BleManager.readRSSI(peripheral.id).then((rssi) => {
                Utils.log('Retrieved actual RSSI value', rssi);
              });
            }); */

              BleManager.requestMTU(peripheral.id, 512)
                .then(mtu => {
                  // Success code
                  Utils.log('MTU size changed to ' + mtu + ' bytes')
                  // Test using bleno's pizza example
                  // https://github.com/sandeepmistry/bleno/tree/master/examples/pizza
                  BleManager.retrieveServices(peripheral.id).then(
                    peripheralInfo => {
                      Utils.log(peripheralInfo)
                      // var service = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
                      // var bakeCharacteristic =
                      //   '6E400002-B5A3-F393-E0A9-E50E24DCCA9E'
                      // var crustCharacteristic =
                      //   '6E400003-B5A3-F393-E0A9-E50E24DCCA9E'

                      setTimeout(() => {
                        BleManager.startNotification(
                            peripheral.id,
                            this.service,
                            this.crustCharacteristic
                          )
                          .then(() => {
                            Utils.log(
                              'Started notification on ' + peripheral.id
                            )
                            // почистить данные сенсоров телефона
                            this.clearDeviceSensorsData()
                            clearInterval(this.state.interval)
                            this.setState({
                              boundedDevice: peripheral,
                              interval: null
                            })
                            Vibration.vibrate([50, 500, 100, 500, 100, 700])
                          })
                          .catch(error => {
                            Utils.log('Notification error', error)
                          })
                      }, 200)
                    }
                  )
                })
                .catch(error => {
                  Utils.log(error)
                })
            }, 900)
          })
          .catch(error => {
            Utils.log('Connection error', error)
          })
      }
    }
  }

  isGyroConnected = gyroId => {
    return new Promise((resolve) => {
      BleManager.isPeripheralConnected(gyroId, []).then(isConnected => {
        if (isConnected) {
          resolve(true)
          Utils.log('Peripheral is connected!')
        } else {
          Utils.log('Peripheral is NOT connected!')
          resolve(false)
        }
      })
    })
  }

  startScan = () => {
    Utils.log(this.state.scanning)
    if (!this.state.scanning) {
      this.props.setPeripherals(new Map())
      // this.setState({ peripherals: new Map() });
      BleManager.scan([], 8, true).then(results => {
        Utils.log('Scanning...')
        this.setState({
          scanning: true
        })
      })
    }
  }

  render() {
    return ( <
      SideBar navigation = {
        this.props.navigation
      }
      bleControls = {
        {
          startBle: this.startBle,
          enableBle: this.enableBle,
          checkStateBle: this.checkStateBle,
          stopBle: this.stopBle,
          startScan: this.startScan,
          retrieveConnected: this.getBondedPeripherals,
          enableDeviceSensors: this.enableDeviceSensors,
          disableDeviceSensors: this.disableDeviceSensors,
          getDeviceSensorsData: this.getDeviceSensorsData,
          testBle: this.testBle,
          getBondedGyro: this.getBondedGyro,
          isGyroConnected: this.isGyroConnected,
          state: this.state
        }
      }
      />
    )
  }
}

export default connect(
  state => state, {
    sendAccelerometerData,
    setPeripherals
  }
)(BleService)