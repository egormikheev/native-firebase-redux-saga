import React, { Component } from "react";

import {
  AppRegistry,
  View,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  StyleSheet
} from "react-native";

import FastImage from 'react-native-fast-image'


import {
  StyleProvider,
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Row,
  Grid,
  Col
} from "native-base";

import firebase from "react-native-firebase";
import logo from "../../../../assets/FitnessTruck.png";

import { RkCard, RkText, RkStyleSheet } from "react-native-ui-kitten";

import Utils from "../../../utils";
import Swiper from "react-native-swiper";
import { DotIndicator } from "react-native-indicators";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

const { width } = Dimensions.get("window");

export default class ProgramInfoScreen extends Component {
  constructor(props) {
    super(props);

    let joined = this.props.navigation.state.params.program.programsMap.join(
      "-"
    );

    this.state = {
      image: null,
      hide: false,
      program: {
        ...this.props.navigation.state.params.program,
        programsMapString: joined,
        programs: this.props.navigation.state.params.program.programs.map(
          pr => {
            return {
              ...pr,
              trains: pr.trains
                ? pr.trains.map(t => {
                    let train = this.props.navigation.state.params.trainings.find(
                      s => s.uri == t.trainRef
                    );
                    return {
                      ...t,
                      ...train
                    };
                  })
                : null
            };
          }
        )
      }
    };
  }

  _showTrainInfoScreen(train) {
    this.props.navigation.navigate("ProgramTrainScreen", {
      train
    });
  }

  componentDidMount() {
    this._getImage().then(image => {
      this.setState({ image, hide: true });
    });
  }

  _getImage = async () => {
    try {
      const ref = firebase.storage().ref(this.state.program.image.stored.name);
      return await ref.getDownloadURL();
    } catch (error) {
      return null;
    }
  };

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <ScrollView style={styles.root}>
            <RkCard rkType="article">
              <View
                style={{
                  display: this.state.hide ? "none" : "flex",
                  height: 250,
                  alignContent: "center",
                  alignItems: "center",
                  backgroundColor: "#EDEDED"
                }}
              >
                <FastImage
                  style={{
                    width: 200,
                    height: 160,
                    marginTop: 15,
                    marginBottom: 25
                  }}
                  source={logo}
                />
                <DotIndicator size={10} color={"#565555"} />
              </View>
              <FastImage
                style={{
                  width: width,
                  height: 250,
                  display: !this.state.hide ? "none" : "flex"
                }}
                source={{ uri: this.state.image }}
              />
              <View rkCardHeader>
                <Grid>
                  <Row>
                    <Text style={styles.header}>
                      {this.state.program.class.toUpperCase()}
                    </Text>
                  </Row>
                  <Row>
                    <Col>
                      <Text style={styles.title}>
                        {this.state.program.header.toUpperCase()}
                      </Text>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Text style={styles.text}>Количество блоков</Text>
                    </Col>
                    <Col>
                      <Text style={styles.textRight}>
                        {String(this.state.program.countPrograms)}
                      </Text>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Text style={styles.text}>Карта упражнений</Text>
                    </Col>
                    <Col>
                      <Text style={styles.textRight}>
                        {this.state.program.programsMapString}
                      </Text>
                    </Col>
                  </Row>
                </Grid>
              </View>
              <View rkCardContent>
                <List style={{ marginRight: 0, paddingRight: 0 }}>
                  {this.state.program.programs.map((p, i) => {
                    if (p.trains && p.trains.length > 0) {
                      return (
                        <Swiper
                          style={{ marginBottom: 5, marginTop: 5 }}
                          key={i}
                          height={100}
                          paginationStyle={styles.pagination_x}
                          activeDotColor={"#565555"}
                        >
                          {p.trains.map((t, ii) => {
                            return (
                              <Grid key={ii} style={{ flex: 1 }}>
                                <Row>
                                  <Col
                                    size={5}
                                    style={{
                                      flexDirection: "column",
                                      alignItems: "flex-end",
                                      justifyContent: "flex-start"
                                    }}
                                  >
                                    <Text
                                      style={styles.textTitle}
                                      onPress={() =>
                                        this._showTrainInfoScreen(t)
                                      }
                                    >
                                      {p.name.toUpperCase()}
                                    </Text>
                                    <Text style={styles.text}>
                                      Группа мышц {t.muscleGroup}
                                    </Text>
                                    <Text
                                      style={styles.text}
                                      onPress={() =>
                                        this._showTrainInfoScreen(t)
                                      }
                                    >
                                      {t.description}
                                    </Text>
                                  </Col>
                                  <Col size={2}>
                                    <FastImage
                                      style={{
                                        width: 100,
                                        height: 100,
                                        display: "flex"
                                      }}
                                      source={{
                                        uri: t.img.maleImg[0]
                                          ? t.img.maleImg[0]
                                          : t.img.femaleImg[0]
                                      }}
                                    />
                                  </Col>
                                </Row>
                              </Grid>
                            );
                          })}
                        </Swiper>
                      );
                    } else {
                      return (
                        <ListItem key={i} style={styles.row}>
                          <Text style={styles.textTitle}>
                            {p.name.toUpperCase()}
                          </Text>
                        </ListItem>
                      );
                    }
                  })}
                </List>
              </View>
            </RkCard>
          </ScrollView>
        </Container>
      </StyleProvider>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base
  },
  title: {
    fontWeight: "600",
    color: "#3d3d3d",
    fontSize: 14
  },
  header: {
    marginBottom: 5,
    fontWeight: "600",
    color: "#565555",
    fontSize: 20
  },
  text: {
    display: "flex",
    fontWeight: "400",
    color: "#3d3d3d",
    alignSelf: "stretch",
    fontSize: 12
  },
  textTitle: {
    display: "flex",
    fontWeight: "600",
    color: "#565555",
    alignSelf: "stretch",
    fontSize: 12
  },
  textRight: {
    display: "flex",
    textAlign: "right",
    color: "#3d3d3d",
    fontSize: 12
  },
  textFullWidth: {
    textAlign: "justify",
    color: "#3d3d3d"
  },
  row: {
    paddingRight: 0,
    paddingLeft: 0,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 0,
    marginRight: 0,
    flex: 1
  },
  pagination_x: {
    position: "absolute",
    bottom: 5,
    left: 0,
    right: 0,
    flexDirection: "row",
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-end",
    backgroundColor: "transparent"
  }
}));
