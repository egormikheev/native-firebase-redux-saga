import React, { Component } from "react";
import {
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  ActivityIndicator
} from "react-native";

import { CachedImage } from "react-native-cached-image";

import {
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  StyleProvider,
  Grid,
  Row,
  Col,
  ListItem,
  Thumbnail,
  Fab,
  View,
  CheckBox
} from "native-base";

import { RkChoice, RkTheme } from "react-native-ui-kitten";
import Utils from "../../../utils";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

import { connect } from "react-redux";
import selector from "../../../redux/selectors/program";

import {
  getMorePrograms,
  toggleProgramToFavorites,
  applyFilterPrograms,
  setNewProgram
} from "../../../redux/actions/userdata";

import logo from "../../../../assets/FitnessTruck.png";

import Filter from "../../../components/program/filterPrograms";

RkTheme.setType("RkChoice", "starSelected", {
  backgroundColor: "transparent",
  borderColor: "transparent",
  alignSelf: "stretch",
  alignContent: "center",
  alignItems: "center",
  display: "flex",
  marginRight: 10,
  inner: {
    imageSource: () => require("../../../../assets/star.png"),
    tintColor: null,
    height: 28,
    width: 28,
    display: "flex"
  }
});

RkTheme.setType("RkChoice", "star", {
  backgroundColor: "transparent",
  borderColor: "transparent",
  display: "flex",
  alignSelf: "stretch",
  alignContent: "center",
  alignItems: "center",
  marginRight: 10,
  inner: {
    imageSource: () => require("../../../../assets/star_unselected.png"),
    tintColor: null,
    height: 28,
    width: 28,
    display: "flex"
  }
});

const { width } = Dimensions.get("window");

class ProgramsListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allPrograms: this.props.filteredPrograms,
      programs: this.props.filteredPrograms.slice(0, 50),
      page: 1,
      size: 50,
      action: false,
      filterActive: false,
      dictionaries: this.props.dictionaries,
      trainings: this.props.trainings
    };

    this._getMorePrograms = this._getMorePrograms.bind(this);
    this._renderItem = this._renderItem.bind(this);
    this._getItemLayout = this._getItemLayout.bind(this);
    this._renderFooter = this._renderFooter.bind(this);
    this._renderHeader = this._renderHeader.bind(this);
  }

  flatList = ref => (this._flatList = ref);

  _showProgramInfoScreen(program) {
    this.props.navigation.navigate("ProgramScreen", {
      program,
      trainings: this.state.trainings
    });
  }

  _addProgram() {
    this.props.navigation.navigate("AddProgram", {
      setNewProgram: this.props.setNewProgram,
      dictionaries: this.state.dictionaries
    });
  }

  _getMorePrograms() {
    const itemsCount = (this.state.page + 1) * this.state.size;
    this.setState({
      programs: this.state.allPrograms.slice(0, itemsCount),
      page: this.state.page + 1
    });
  }

  _getItemLayout = (data, index) => ({
    length: 110.3,
    offset: 110.3 * index,
    index
  });

  _toggleProgramToFavorites(itemId, isFavorite) {
    let newPrograms = this.state.programs.map(tr => {
      if (tr.id === itemId) {
        tr.isFavorite = isFavorite;
      }
      return tr;
    });
    this.setState({
      programs: newPrograms,
      action: "toggleFavorite"
    });
    this.props.toggleProgramToFavorites(itemId, isFavorite);
  }

  _renderHeader() {
    return <Filter {...this.props} />;
  }

  _renderFooter = () => {
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="small" />
      </View>
    );
  };

  _renderItem({ item, index }) {
    return (
      <ListItem key={index} style={styles.item}>
        <CachedImage
          style={
            item.image && item.image.href
              ? styles.thumbnail
              : styles.thumbnailLogo
          }
          source={
            item.image && item.image.href
              ? {
                  uri: item.image.href
                }
              : logo
          }
        />
        <TouchableWithoutFeedback>
          <Grid>
            <Row alignItems="center">
              <Col
                onPress={() => {
                  this._showProgramInfoScreen(item);
                }}
                size={5}
              >
                <Body style={{ justifyContent: "center" }}>
                  <Text style={styles.textHeader}>{item.class}</Text>
                  <Text note>{item.header}</Text>
                </Body>
              </Col>
              <Col size={1} alignItems="center">
                <RkChoice
                  rkType="star"
                  onChange={() => {
                    this._toggleProgramToFavorites(item.id, !item.isFavorite);
                  }}
                  selected={item.isFavorite}
                />
              </Col>
            </Row>
          </Grid>
        </TouchableWithoutFeedback>
      </ListItem>
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.action === "setFilter") {
      const itemsCount = this.state.page * this.state.size;
      this.setState({
        filterActive: true,
        programs: nextProps.filteredPrograms.slice(0, itemsCount),
        allPrograms: nextProps.filteredPrograms
      });
    }
    if (nextProps.action == "resetFilter") {
      const itemsCount = this.state.page * this.state.size;
      this.setState({
        filterActive: false,
        programs: nextProps.filteredPrograms.slice(0, itemsCount),
        allPrograms: nextProps.filteredPrograms
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextState.programs.length != this.state.programs.length ||
      nextState.action === "toggleFavorite" ||
      this.props.favorites.programs.length !=
        nextProps.favorites.programs.length
    ) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={{ flex: 1 }} style={styles.container}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Icon style={{ color: 'black' }} name='menu' />
              </Button>
            </Left>
            <Body>
              <Title style={styles.headerTitle}>Каталог программ</Title>
            </Body>
            <Right>
              <Button transparent onPress={() => this._addProgram()}>
                <Icon type="Entypo" name="squared-plus" />
              </Button>
            </Right>
          </Header>
          <FlatList
            ref={ref => {
              this.flatListRef = ref;
            }}
            data={this.state.programs}
            onEndReachedThreshold={0.5}
            onEndReached={this._getMorePrograms}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderItem}
            initialNumToRender={50}
            maxToRenderPerBatch={50}
            ListHeaderComponent={this._renderHeader}
            ListFooterComponent={this._renderFooter}
          />
          <Fab
            direction="up"
            containerStyle={{}}
            style={{ backgroundColor: "#7546D7" }}
            position="bottomRight"
            onPress={() => {
              this.flatListRef.scrollToIndex({ animated: true, index: 0 });
            }}
          >
            <Icon name="ios-arrow-up" />
          </Fab>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white"
  },
  item: {
    backgroundColor: "white",
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 10,
    paddingRight: 0
  },
  right: {
    display: "flex",
    backgroundColor: "white",
    alignItems: "center",
    padding: 0,
    margin: 0
  },
  thumbnail: {
    width: 100,
    height: 100
  },
  thumbnailLogo: {
    width: 80,
    height: 65,
    marginTop: 10,
    marginBottom: 20,
    top: 0
  },
  headerTitle: {
    width: width - 150,
    textAlign: "left"
  },
  textHeader: {
    fontWeight: "600",
    color: "#EDEDED"
  },
  cell: {
    alignItems: "center"
  }
});

export default connect(
  state => selector(state),
  {
    getMorePrograms,
    toggleProgramToFavorites,
    applyFilterPrograms,
    setNewProgram
  }
)(ProgramsListPage);
