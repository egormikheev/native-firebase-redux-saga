import React from "react";

import { AppRegistry, View, StatusBar } from "react-native";
import {
  StyleProvider,
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Row,
  Grid,
  Col
} from "native-base";

import FilterOptions from "../../../components/program/filterOptionsScreen";

import Utils from "../../../utils";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

export default class OptionsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: undefined
    };
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="arrow-round-back" />
              </Button>
            </Left>
            <Body />
            <Right>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="add" />
              </Button>
            </Right>
          </Header>
          <FilterOptions {...this.props} />
        </Container>
      </StyleProvider>
    );
  }
}
