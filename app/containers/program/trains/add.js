import React from "react";

import { AppRegistry, View, StatusBar } from "react-native";
import {
  StyleProvider,
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Row,
  Grid,
  Col,
  ActionSheet
} from "native-base";

import { RkTextInput, RkAvoidKeyboard } from "react-native-ui-kitten";

import Utils from "../../../utils";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

export default class AddTrainPage extends React.Component {
  constructor(props) {
    super(props);

    const prop = this.props.navigation.state.params;
    let dictionaries = prop.dictionaries;
    let keys = Object.keys(dictionaries).reduce((a, c) => {
      a[c] = "Не указано";
      return a;
    }, {});

    for (let dict in dictionaries) {
      if (dictionaries[dict][0].name != "Не указано")
        dictionaries[dict].unshift({ name: "Не указано" });
    }

    this.state = {
      checked: keys,
      dictionaries: dictionaries,
      activityDescription: "",
      activityName: ""
    };

    this._renderOptions = this._renderOptions.bind(this);
    this._renderSelectors = this._renderSelectors.bind(this);
    this._addNewTrain = this._addNewTrain.bind(this);
    this._addTrain = this.props.navigation.state.params.setNewTrain.bind(this);
  }

  _addNewTrain() {
    this._addTrain({
      ...this.state.checked,
      header: this.state.activityName,
      steps: this.state.activityDescription
    });
    this.props.navigation.goBack();
  }

  _renderSelectors() {
    return Object.entries(this.state.checked).map(s => {
      return (
        <Button
          key={s[0]}
          full
          style={{ marginBottom: 10, marginTop: 10 }}
          onPress={() =>
            this._renderOptions(
              s[0],
              this.state.dictionaries[s[0]].map(s => s.name)
            )
          }
        >
          <Text>{s[0] + " : " + s[1]}</Text>
        </Button>
      );
    });
  }

  _renderOptions(name, dictionary) {
    ActionSheet.show(
      {
        options: dictionary,
        cancelButtonIndex: 3,
        destructiveButtonIndex: 4,
        title: "Выбрать группу мышц"
      },
      buttonIndex => {
        this.setState({
          ...this.state,
          checked: {
            ...this.state.checked,
            [name]: dictionary[buttonIndex]
          }
        });
      }
    );
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="arrow-round-back" />
              </Button>
            </Left>
            <Body />
            <Right>
              <Button transparent onPress={this._addNewTrain}>
                <Icon type={"Entypo"} name="save" />
              </Button>
            </Right>
          </Header>
          <Content padder>
            <RkAvoidKeyboard>
              <RkTextInput
                placeholder="Название активности"
                onChangeText={activityName =>
                  this.setState({ ...this.state, activityName })
                }
                value={this.state.activityName}
                inputStyle={{
                  backgroundColor: "lightgray",
                  color: "black",
                  paddingLeft: 10,
                  marginLeft: 0
                }}
                style={{ marginRight: 0, marginLeft: 0 }}
              />

              <RkTextInput
                placeholder="Описание активности"
                onChangeText={activityDescription =>
                  this.setState({ ...this.state, activityDescription })
                }
                value={this.state.activityDescription}
                inputStyle={{
                  backgroundColor: "lightgray",
                  color: "black",
                  paddingLeft: 10,
                  marginLeft: 0
                }}
                multiline={true}
                numberOfLines={4}
                style={{ marginRight: 0, marginLeft: 0 }}
              />
            </RkAvoidKeyboard>
            {this._renderSelectors()}
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}
