import React, { Component } from "react";

import {
  AppRegistry,
  View,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  StyleSheet
} from "react-native";

import FastImage from 'react-native-fast-image'

import {
  StyleProvider,
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Row,
  Grid,
  Col
} from "native-base";

import firebase from "react-native-firebase";
import logo from "../../../../assets/FitnessTruck.png";

import { RkCard, RkText, RkStyleSheet } from "react-native-ui-kitten";

import Utils from "../../../utils";
import Swiper from "react-native-swiper";
import { DotIndicator } from "react-native-indicators";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

class SwiperWrapper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      hide: false
    };

    this.state.images = this.props.images ? this.props.images : [];
  }

  componentDidUpdate(prevProps) {
    if (prevProps.images.length != this.props.images.length) {
      this.setState({ images: this.props.images, hide: true });
    }
  }

  render() {
    return (
      this.state.images &&
      this.state.images.length > 0 && (
        <Swiper height={250} key="swiper">
          {this.state.images.map((item, index) => {
            return (
              <FastImage
                style={{
                  height: 250
                }}
                key={item}
                source={{
                  uri: item
                }}
              />
            );
          })}
        </Swiper>
      )
    );
  }
}

export default class OptionsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: undefined,
      images: [],
      hide: false
    };
    this.state.train = this.props.navigation.state.params.train;
  }

  componentDidMount() {
    this._getImages().then(images => {
      this.setState({ images, hide: true });
    });
  }

  _getImages = async () => {
    try {
      let images = [];
      for (let image of this.state.train.img.stored) {
        const path = image.url.split(".com/")[2];
        const ref = firebase.storage().ref(path);
        const uri = await ref.getDownloadURL();
        images.push(uri);
      }
      return images;
    } catch (error) {
      return [];
    }
  };

  render() {
    const { train } = this.props.navigation.state.params;
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <ScrollView style={styles.root}>
            <RkCard rkType="article">
              <View
                style={{
                  display: this.state.hide ? "none" : "flex",
                  height: 250,
                  alignContent: "center",
                  alignItems: "center",
                  backgroundColor: "#EDEDED"
                }}
              >
                <FastImage
                  style={{
                    width: 200,
                    height: 160,
                    marginTop: 35,
                    marginBottom: 15
                  }}
                  source={logo}
                />
                <DotIndicator size={10} color={"#565555"}  />
              </View>
              <SwiperWrapper images={this.state.images} />

              <View rkCardHeader>
                <Grid>
                  <Row>
                    <Text style={styles.title}>{train.header}</Text>
                  </Row>
                  <Row>
                    <Col>
                      <Text style={styles.text}>Группа мышц:</Text>
                    </Col>
                    <Col>
                      <Text style={styles.textRight}>{train.muscleGroup}</Text>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Text style={styles.text}>Оборудование:</Text>
                    </Col>
                    <Col>
                      <Text style={styles.textRight}>{train.equipment}</Text>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Text style={styles.text}>Тип тренировки:</Text>
                    </Col>
                    <Col>
                      <Text style={styles.textRight}>{train.typeTrain}</Text>
                    </Col>
                  </Row>
                </Grid>
              </View>
              <View rkCardContent>
                <View>
                  <Text style={styles.title} rkType="header">
                    Порядок выполнения
                  </Text>
                  <View>
                    {train.steps.map((item, i) => {
                      return (
                        <Text key={i} style={styles.textFullWidth}>
                          {(++i).toString()} ) {item}
                        </Text>
                      );
                    })}
                  </View>
                </View>
              </View>
            </RkCard>
          </ScrollView>
        </Container>
      </StyleProvider>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base
  },
  title: {
    marginBottom: 5,
    fontWeight: "600",
    color: "#565555"
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border.base,
    alignItems: "stretch"
  },
  text: {
    display: "flex",
    fontWeight: "400",
    color: "#3d3d3d"
  },
  textRight: {
    display: "flex",
    textAlign: "right",
    color: "#3d3d3d"
  },
  textFullWidth: {
    textAlign: "justify",
    color: "#3d3d3d"
  }
}));
