import React, { Component } from 'react'
import {
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  View,
  Platform
} from 'react-native'

import FastImage from 'react-native-fast-image'
import {
  RecyclerListView,
  DataProvider,
  LayoutProvider
} from 'recyclerlistview'

import {
  Container,
  Body,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  StyleProvider,
  Grid,
  Row,
  Col,
  ListItem,
  Fab
} from 'native-base'

import { RkChoice, RkTheme } from 'react-native-ui-kitten'
import Utils from '../../../utils'

import getTheme from '../../../../native-base-theme/components'
import material from '../../../../native-base-theme/variables/material'

import { connect } from 'react-redux'
import selector from '../../../redux/selectors/userdata'

import {
  getMoreTrainings,
  toggleTrainToFavorites,
  applyFilter,
  setNewTrain
} from '../../../redux/actions/userdata'

import logo from '../../../../assets/FitnessTruck.png'

import Filter from '../../../components/program/filter'

RkTheme.setType('RkChoice', 'starSelected', {
  backgroundColor: 'transparent',
  borderColor: 'transparent',
  alignSelf: 'stretch',
  alignContent: 'center',
  alignItems: 'center',
  display: 'flex',
  marginRight: 10,
  inner: {
    imageSource: () => require('../../../../assets/star.png'),
    tintColor: null,
    height: 28,
    width: 28,
    display: 'flex'
  }
})

RkTheme.setType('RkChoice', 'star', {
  backgroundColor: 'transparent',
  borderColor: 'transparent',
  display: 'flex',
  alignSelf: 'stretch',
  alignContent: 'center',
  alignItems: 'center',
  marginRight: 10,
  inner: {
    imageSource: () => require('../../../../assets/star_unselected.png'),
    tintColor: null,
    height: 28,
    width: 28,
    display: 'flex'
  }
})

const { width } = Dimensions.get('window');

const layoutProvider = new LayoutProvider(
  (index) => {
    if (index === 0) return 'ROW0'
    return 'ROW'
  },
  (type, dim) => {
    dim.width = Dimensions.get('window').width;
    if(type === 'ROW0') dim.height = 110
    else {
    dim.height = 110
    }
  }
);

const dataProvider = new DataProvider((r1, r2) => {
  return r1 !== r2
})

const disableRecycling = Platform.select({
  ios: false,
  android: false
})

class TrainsListPage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      dataProvider,
      layoutProvider,
      allTrainings: this.props.userdata.filteredTrainings,
      trainings: [],
      page: 1,
      size: 20,
      action: false,
      filterActive: false,
      dictionaries: this.props.userdata.dictionaries,
      inProgressLoad: false,
    }
    this._getMoreTrainings = this._getMoreTrainings.bind(this)
    this._renderItem = this._renderItem.bind(this)
    this._renderFooter = this._renderFooter.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._renderAndroid = this._renderAndroid.bind(this)
    this._toggleFilter = this._toggleFilter.bind(this)
  }

  componentDidMount () {
    this._getMoreTrainings()
  }

  _toggleFilter() {
    this.setState({ filterActive: !this.state.filterActive })
  }

  _showTrainInfoScreen (train) {
    this.props.navigation.navigate('TrainScreen', {
      train
    })
  }

  _addTrain () {
    this.props.navigation.navigate('AddTrain', {
      setNewTrain: this.props.setNewTrain,
      dictionaries: this.state.dictionaries
    })
  }

  _getMoreTrainings () {
    this.setState({ inProgressLoad: true })
    const itemsCount = (this.state.page + 1) * this.state.size
    const newTrains = this.state.allTrainings.slice(0, itemsCount)
    this.setState({
      dataProvider: dataProvider.cloneWithRows(newTrains),
      trainings: newTrains,
      page: this.state.page + 1,
      inProgressLoad: true
    })
  }

  _toggleTrainToFavorites (itemId, isFavorite) {
    let newTrainings = this.state.trainings.map(tr => {
      if (tr.id === itemId) {
        tr.isFavorite = isFavorite
      }
      return tr
    })
    this.setState({
      trainings: newTrainings,
      action: 'toggleFavorite'
    })
    this.props.toggleTrainToFavorites(itemId, isFavorite)
  }

  _renderHeader () {
    return <Filter {...this.props} filterActive={ this.state.filterActive } ref={ (ref) => this.filter = ref } />
  }

  _renderFooter = () => {
    return (
      this.state.inProgressLoad &&
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE'
        }}
      >
        <ActivityIndicator animating size='small' />
      </View>
    )
  };

  _renderItem = Platform.select({
    ios: this._renderAndroid,
    android: this._renderAndroid
  });

  _renderAndroid (type, item) {
    return (
      <ListItem style={styles.item}>
        <FastImage
          onPress={() => {
            this._showTrainInfoScreen(item)
          }}
          style={
            item.img && item.img.maleImg[0]
              ? styles.thumbnail
              : styles.thumbnailLogo
          }
          source={
            item.img && item.img.maleImg[0]
              ? {
                uri: item.img.maleImg[0].replace('300x200', '150x100')
              }
              : logo
          }
        />

        <Grid>
          <Row alignItems='center'>
            <Col
              onPress={() => {
                this._showTrainInfoScreen(item)
              }}
              size={5}
            >
              <Body style={{ justifyContent: 'center' }}>
                <Text style={styles.textHeader}>{item.muscleGroup}</Text>
                <Text note>{item.header}</Text>
              </Body>
            </Col>
            <Col size={1} alignItems='center'>
              <RkChoice
                rkType='star'
                onChange={() => {
                  this._toggleTrainToFavorites(item.id, !item.isFavorite)
                }}
                selected={item.isFavorite}
              />
            </Col>
          </Row>
        </Grid>
      </ListItem>
    )
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.userdata.action === 'setFilter') {
      const itemsCount = this.state.page * this.state.size
      this.setState({
        filterActive: true,
        trainings: nextProps.userdata.filteredTrainings.slice(0, itemsCount),
        allTrainings: nextProps.userdata.filteredTrainings
      })
    }
    if (nextProps.userdata.action == 'resetFilter') {
      const itemsCount = this.state.page * this.state.size
      this.setState({
        filterActive: false,
        trainings: nextProps.userdata.filteredTrainings.slice(0, itemsCount),
        allTrainings: nextProps.userdata.filteredTrainings
      })
    }
  }

  // shouldComponentUpdate (nextProps, nextState) {
  //   if (
  //     nextState.trainings.length != this.state.trainings.length ||
  //     nextState.action === 'toggleFavorite' ||
  //     this.props.userdata.favorites.trainings.length !=
  //       nextProps.userdata.favorites.trainings.length
  //   ) {
  //     return true
  //   }
  //   return false
  // }

  // componentDidUpdate () {
  //   this.setState({ trainings: this.props.userdata.filteredTrainings })
  // }

  render () {
    const { dataProvider, layoutProvider } = this.state

    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={{ flex: 1 }} style={styles.container}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Icon style={{ color: 'black' }} name='menu' />
              </Button>
            </Left>
            <Body>
              <Title style={styles.headerTitle}>Каталог упражнений</Title>
            </Body>
            <Right>
            <Button transparent onPress={() => this._toggleFilter() }>
                <Icon style={{color: "#565555" }} type='FontAwesome' name='filter' />
              </Button>
              <Button transparent onPress={() => this._addTrain()}>
                <Icon style={{color: "#565555" }} type='Entypo' name='squared-plus' />
              </Button>
            </Right>
          </Header>
          { this._renderHeader() }
          <RecyclerListView
            ref={ref => {
              this.flatListRef = ref
            }}
            onEndReached={this._getMoreTrainings}
            dataProvider={dataProvider}
            layoutProvider={layoutProvider}
            rowRenderer={this._renderAndroid}
            renderFooter={this._renderFooter}
            disableRecycling={disableRecycling}
          />
        </Container>
      </StyleProvider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white'
  },
  item: {
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 10,
    paddingRight: 0
  },
  right: {
    display: 'flex',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 0,
    margin: 0
  },
  thumbnail: {
    width: 100,
    height: 100
  },
  thumbnailLogo: {
    width: 80,
    height: 65,
    marginTop: 10,
    marginBottom: 20,
    top: 0
  },
  headerTitle: {
    width: width - 150,
    textAlign: 'left'
  },
  textHeader: {
    fontWeight: '600',
    color: '#565555'
  },
  cell: {
    alignItems: 'center'
  }
})

export default connect(state => selector(state), {
  getMoreTrainings,
  toggleTrainToFavorites,
  applyFilter,
  setNewTrain
})(TrainsListPage)
