import React from 'react'
import { StyleSheet } from 'react-native'

import {
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  StyleProvider,
  Grid,
  Row,
  Col
} from 'native-base'

import l from 'lodash'
import Utils from '../../../utils'
import SettingsPage from '../../../components/profile/settings'

import getTheme from '../../../../native-base-theme/components'
import material from '../../../../native-base-theme/variables/material'

export default class MenuPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left>
              <Button
                
                transparent
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Icon style={{ color: 'black' }} name='menu' />
              </Button>
            </Left>
            <Body>
              <Title>Настройки</Title>
            </Body>
            <Right />
          </Header>
          <SettingsPage {...this.props} />
        </Container>
      </StyleProvider>
    )
  }
}

const styles = StyleSheet.create({
  item: {
    color: 'white'
  }
})

// export default connect(state => selector(state), {
//   getCalendarDays
// })(MainPage)
