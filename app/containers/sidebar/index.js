import React, { Component } from 'react'
import { Image, StyleSheet, TouchableOpacity } from 'react-native'
import { NavigationActions, DrawerActions } from 'react-navigation'
import firebase from 'react-native-firebase'
import Accordion from 'react-native-collapsible/Accordion'
import logo from '../../../assets/FitnessTruck.png'
import Utils from '../../utils'

import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Grid,
  Row,
  View,
  Icon,
  Col
} from 'native-base'

const routes = [
  {
    router: 'Program',
    title: 'Каталог',
    icon: { name: 'book', type: 'Entypo' },
    views: [
      {
        view: 'trains',
        name: 'Упражнения',
        icon: { name: 'alpha', type: 'MaterialCommunityIcons' }
      },
      {
        view: 'programs',
        name: 'Программы',
        icon: { name: 'alphabetical', type: 'MaterialCommunityIcons' }
      },
      {
        view: 'favorites',
        name: 'Избранное',
        icon: { name: 'bookmark', type: 'FontAwesome' }
      }
    ]
  },
  {
    router: 'Train',
    title: 'Трекер',
    icon: { name: 'road', type: 'FontAwesome' },
    views: [
      {
        view: 'main',
        name: 'Прогресс',
        icon: { name: 'event-note', type: 'MaterialIcons' }
      },
      {
        view: 'train',
        name: 'Тренировка',
        icon: { name: 'walk', type: 'MaterialCommunityIcons' }
      },
      {
        view: 'settings',
        name: 'Сопряжение',
        icon: { name: 'watch', type: 'Feather' }
      }
    ]
  },
  {
    router: 'Profile',
    title: 'Мой профиль',
    icon: { name: 'id-card-o', type: 'FontAwesome' },
    views: [
      {
        view: 'user',
        name: 'Профиль',
        icon: { name: 'user', type: 'FontAwesome' }
      },
      {
        view: 'history',
        name: 'Программы',
        icon: { name: 'notebook', type: 'SimpleLineIcons' }
      },
      {
        view: 'settings',
        name: 'Настройки',
        icon: { name: 'md-settings', type: 'Ionicons' }
      }
    ]
  }
]

export default class SideBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      activeSections: [],
      collapsed: true,
      bluetoothEnabed: false
    }
  }

  toggleBluetooth = () => {
    const { state, enableBle } = this.props.bleControls
    if (state.bleState == 'off' || state.scanning == false) {
      enableBle()
    }
  }

  toggleDeviceSensors = duration => {
    const {
      state,
      enableDeviceSensors,
      disableDeviceSensors
    } = this.props.bleControls

    if (state.deviceSensorsState) {
      disableDeviceSensors()
    } else {
      enableDeviceSensors(duration)
    }
  }

  signOut = () => {
    firebase
      .auth()
      .signOut()
      .then(function () {
        // Sign-out successful.
      })
      .catch(function (error) {
        // An error happened.
      })
  }
  _setSection = activeSections => {
    this.setState({ activeSections })
  }

  _navigate = (router, view) => {
    Utils.log({ router, view })
    this.props.navigation.navigate({
      routeName: router,
      params: { ...this.props },
      action: NavigationActions.navigate({
        routeName: view,
        params: { ...this.props }
      })
    })

    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  _renderHeader = (section, i, isActive) => {
    return (
      <View style={[styles.header, isActive ? styles.active : styles.inactive]}>
        <Icon
          style={styles.icon}
          name={section.icon.name}
          type={section.icon.type}
        />
        <Text style={styles.text}>{section.title}</Text>
      </View>
    )
  }

  _renderContent = section => {
    return (
      <List
        dataArray={section.views}
        renderRow={data => {
          return (
            <ListItem
              button
              onPress={() => {
                this._navigate(section.router, data.view)
              }}
            >
              <Icon
                style={styles.icon}
                name={data.icon.name}
                type={data.icon.type}
              />
              <Text style={styles.text}>{data.name}</Text>
            </ListItem>
          )
        }}
      />
    )
  }

  render () {
    const {
      scanning,
      bleState,
      deviceSensorsState
    } = this.props.bleControls.state

    let bleStatus = { text: 'Выключен', icon: 'bluetooth-disabled' }
    let deviceSensorsStatus = { text: 'Выключен', icon: 'microchip' }
    if (bleState == 'on' && scanning == true) {
      bleStatus = { text: 'Сканирую', icon: 'bluetooth-searching' }
    }

    if (bleState == 'on') {
      bleStatus = { text: 'Включен', icon: 'bluetooth' }
    }

    if (deviceSensorsState) {
      deviceSensorsStatus = { text: 'Включен', icon: 'microchip' }
    }

    return (
      <Container>
        <Content>
          <Grid>
            <Row
              style={{
                backgroundColor: '#EDEDED',
                height: 200,
                alignSelf: 'stretch',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={logo}
                style={{
                  height: 110,
                  width: 135
                }}
              />
            </Row>
            <Row>
              <Accordion
                activeSections={this.state.activeSections}
                sections={routes}
                renderHeader={this._renderHeader.bind(this)}
                renderContent={this._renderContent.bind(this)}
                navigate={this._navigate}
                // touchableComponent={TouchableOpacity}
                onChange={this._setSection.bind(this)}
                bleControls={this.props.bleControls}
                // renderSectionTitle={this._renderContent.bind(this)}
              />
            </Row>
            <Row>
              <List>
                <ListItem key={'ble'} button onPress={this.toggleBluetooth}>
                  <Icon
                    style={styles.iconFirst}
                    name={bleStatus.icon}
                    type={'MaterialIcons'}
                  />
                  <Text style={styles.text}>{bleStatus.text}</Text>
                </ListItem>
                <ListItem
                  key={'device'}
                  button
                  onPress={() => this.toggleDeviceSensors(100)}
                >
                  <Icon
                    style={styles.iconFirst}
                    name={deviceSensorsStatus.icon}
                    type={'FontAwesome'}
                  />
                  <Text style={styles.text}>{deviceSensorsStatus.text}</Text>
                </ListItem>
                <ListItem key={'logout'} button onPress={this.signOut}>
                  <Icon
                    style={styles.iconFirst}
                    name={'power-off'}
                    type={'FontAwesome'}
                  />
                  <Text style={styles.text}>Выйти</Text>
                </ListItem>
              </List>
            </Row>
          </Grid>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    width: 400,
    backgroundColor: '#ececec',
    display: 'flex',
    flexDirection: 'row'
  },
  active: {
    backgroundColor: '#d8c8f9',
    borderBottomColor: '#7546D7',
    borderBottomWidth: 6
  },
  inactive: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
  icon: {
    display: 'flex',
    fontSize: 18,
    marginRight: 10,
    color: '#7546D7'
  },

  iconFirst: {
    display: 'flex',
    fontSize: 18,
    marginLeft: 5,
    marginRight: 10,
    color: '#7546D7'
  },

  text: {
    display: 'flex',
    fontSize: 16,
    color: '#7546D7',
    width: 400
  }
})
