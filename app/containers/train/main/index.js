import React from "react";
import { StyleSheet } from "react-native";
import {
  Container,
  Body,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  StyleProvider,
  Grid,
  Row,
  Col
} from "native-base";

import m from "moment";

import { Agenda, LocaleConfig } from "react-native-calendars";
import Utils from "../../../utils";

LocaleConfig.locales["ru"] = Utils.locale("ru");
LocaleConfig.defaultLocale = "ru";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

import { getCalendarDays } from "../../../redux/actions/calendar";
import { connect } from "react-redux";
import selector from "../../../redux/selectors/calendar";

Array.prototype.randomElement = function() {
  return this[Math.floor(Math.random() * this.length)];
};

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {}
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(prevState.items) != JSON.stringify(this.props.items)) {
      this.setState({
        items: this.props.items
      });
    }
  }

  renderTrain(train) {
    return (
      <Container
        style={[
          styles.item,
          {
            height: train.countActoins * 20,
            backgroundColor: "#8F6DD7",
            borderRadius: 2
          }
        ]}
      >
        <Text style={styles.title}>Тренировка: {train.formatDuration}</Text>
        {train.actions.map((item, key) => this.renderItem(item, key))}
      </Container>
    );
  }

  renderItem(item, key) {
    return (
      <Grid key={key} style={{ backgroundColor: "white" }}>
        <Row style={[styles.cardHeader]}>
          <Text style={styles.cardTitle}>
            {item.activity.header.toUpperCase()}
          </Text>
        </Row>
        <Row style={{ margin: 10, alignItems: "center" }}>
          <Col size={1}>
            <Icon
              name={"metronome-tick"}
              type={"MaterialCommunityIcons"}
              style={styles.cardItemIcon}
            />
          </Col>
          <Col size={1}>
            <Text style={styles.cardItem}>{item.totalSwaps}</Text>
          </Col>
          <Col size={1}>
            <Icon style={styles.cardItemIcon} name={"md-stopwatch"} />
          </Col>
          <Col size={10}>
            <Text style={styles.cardItem}>
              {m.duration(item.totalTime, "milliseconds").format("hh:mm:ss", {
                trim: false
              })}{" "}
              {`c`}
            </Text>
          </Col>
        </Row>
      </Grid>
    );
  }

  render() {
    const { getCalendarDays, navigation } = this.props;

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={() => navigation.openDrawer()}>
                <Icon style={{ color: 'black' }} name='menu' />
              </Button>
            </Left>
            <Body>
              <Title>прогресс</Title>
            </Body>
            <Right />
          </Header>
          <Agenda
            items={this.state.items}
            loadItemsForMonth={getCalendarDays.bind(this)}
            selected={new Date().getTime()}
            renderItem={this.renderTrain.bind(this)}
            renderEmptyDate={this.renderEmptyDate.bind(this)}
            rowHasChanged={this.rowHasChanged.bind(this)}
            theme={{
              backgroundColor: "#f0f0f0",
              calendarBackground: "#d3d3d3",
              textSectionTitleColor: "#565656",
              selectedDayBackgroundColor: "#7546D7",
              selectedDayTextColor: "#ffffff",
              todayTextColor: "#f2f2f2",
              dayTextColor: "#565656",
              textDisabledColor: "#d9e1e8",
              dotColor: "#ffffff",
              selectedDotColor: "#ffffff",
              arrowColor: "orange",
              monthTextColor: "#7546D7",
              textMonthFontWeight: "bold",
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 16,
              agendaKnobColor: "#ffffff",
              agendaTodayColor: "#7546D7"
            }}
          />
        </Container>
      </StyleProvider>
    );
  }

  renderEmptyDate() {
    return (
      <Container style={styles.emptyDate}>
        <Text />
      </Container>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }
}

const styles = StyleSheet.create({
  cardTitle: {
    fontWeight: "600",
    fontSize: 14,
    color: "#7546D7",
    paddingBottom: 5
  },
  cardHeader: {
    borderBottomColor: "#f0f0f0",
    borderBottomWidth: 1,
    margin: 10
  },
  item: {
    backgroundColor: "#ffffff",
    flex: 1,
    borderRadius: 1,
    marginRight: 10,
    marginTop: 17
  },
  cardItem: {
    fontSize: 16,
    color: "#5C5C5C"
  },
  cardItemIcon: {
    fontSize: 18,
    color: "#5C5C5C"
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  },
  title: {
    color: "white",
    margin: 10
  }
});

export default connect(
  state => selector(state),
  {
    getCalendarDays
  }
)(MainPage);
