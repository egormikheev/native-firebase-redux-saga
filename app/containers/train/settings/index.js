import React from 'react'
import Utils from '../../../utils'
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  Dimensions
} from 'react-native'
import {
  Container,
  Body,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text
} from 'native-base'

import { Base64 } from 'js-base64'

import { connect } from 'react-redux'
import selector from '../../../redux/selectors/sensorsWorker'

import * as Buffer from 'buffer'

const window = Dimensions.get('window')
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

class TrainPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      scanning: false,
      peripherals: new Map(),
      appState: ''
    }
  }

  componentDidMount () {}

  componentWillReceiveProps (nextProps) {
    Utils.log({ newProps: nextProps })
  }

  shouldComponentUpdate (nextProps) {
    if (
      nextProps.navigation.state.params &&
      nextProps.navigation.state.params.bleControls
    ) {
      return true
    }
    return false
  }

  _renderRow (item) {
    const color = item.connected ? 'green' : '#fff'
    return (
      <TouchableHighlight
        key={item.id}
        onPress={() =>
          this.props.navigation.state.params.bleControls.testBle(item)
        }
      >
        <View style={[styles.row, { backgroundColor: color }]}>
          <Text
            style={{
              fontSize: 12,
              textAlign: 'center',
              color: '#333333',
              padding: 10
            }}
          >
            {item.name}
          </Text>
          <Text
            style={{
              fontSize: 8,
              textAlign: 'center',
              color: '#333333',
              padding: 10
            }}
          >
            {item.id}
          </Text>
        </View>
      </TouchableHighlight>
    )
  }

  render () {
    console.log('rerender')
    const { startScan, retrieveConnected, state } = this.props.navigation.state
      .params
      ? this.props.navigation.state.params.bleControls
      : {
        startScan: () => {},
        undefined,
        undefined,
        state: { scanning: false }
      }

    const list = Array.from(
      this.props.peripherals instanceof Map
        ? this.props.peripherals.values()
        : []
    )

    const dataSource = ds.cloneWithRows(list)

    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon style={{ color: 'black' }} name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>настройки</Title>
          </Body>
          <Right />
        </Header>
        <View style={styles.container}>
          <TouchableHighlight
            style={{
              marginTop: 40,
              margin: 20,
              padding: 20,
              backgroundColor: '#ccc'
            }}
            onPress={() => startScan()}
          >
            <Text>Scan Bluetooth ({state.scanning ? 'on' : 'off'})</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={{
              marginTop: 0,
              margin: 20,
              padding: 20,
              backgroundColor: '#ccc'
            }}
            onPress={retrieveConnected}
          >
            <Text>Retrieve connected peripherals</Text>
          </TouchableHighlight>
          <ScrollView style={styles.scroll}>
            {list.length == 0 && (
              <View style={{ flex: 1, margin: 20 }}>
                <Text style={{ textAlign: 'center' }}>No peripherals</Text>
              </View>
            )}
            {list.length !== 0 &&
              list.map(item => {
                return this._renderRow(item)
              })}
            {/* <ListView
              enableEmptySections
              dataSource={dataSource}
              renderRow={this._renderRow.bind(this)}
            /> */}
          </ScrollView>
        </View>
      </Container>
    )
  }
}

export default connect(
  state => selector(state),
  {}
)(TrainPage)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    width: window.width,
    height: window.height
  },
  scroll: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    margin: 10
  },
  row: {
    margin: 10
  }
})
