import React from "react";
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Dimensions
} from "react-native";

import LinearGradient from "react-native-linear-gradient";

import { connect } from "react-redux";

import {
  toggleRecord,
  stopRecord,
  swapDirection,
  selectTrain,
  startTraining,
  stopTraining,
  startProgram,
  pauseProgram,
  stopProgram
} from "../../../redux/actions/training";

import { CachedImage } from "react-native-cached-image";

import { sendActivity } from "../../../redux/actions/timer";
import selector from "../../../redux/selectors/training";

import {
  StyleProvider,
  Container,
  Body,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  Grid,
  Col,
  Row,
  List,
  ListItem
} from "native-base";

import { RkChoice, RkTheme } from "react-native-ui-kitten";

import Swiper from "@nart/react-native-swiper";
import * as Animatable from "react-native-animatable";

import Recorder from "../../../components/training/Recorder";
import Monitor from "../../../components/training/Monitor";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";

import UUIDGenerator from "react-native-uuid-generator";

import Utils from "../../../utils";

const { width } = Dimensions.get("window");

class TrainPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isFreeTrain: false,
      isProgramTrain: false
    };
  }

  _chooseTypeTrain(type) {
    if (type == "free") {
      this.setState({
        isFreeTrain: !this.state.isFreeTrain,
        isProgramTrain: false
      });
    } else {
      this.setState({
        isFreeTrain: false,
        isProgramTrain: !this.state.isProgramTrain
      });
    }
  }

  handleViewRef = ref => (this.view = ref);

  swiperRef = ref => (this._swiper = ref);

  startTraining = () =>
    this.view.bounce(800).then(endState => {
      Utils.log({ STATE: this.state });
      if (this.state.isFreeTrain) {
        this._swiper.scrollEnabled = true;
        UUIDGenerator.getRandomUUID().then(uuid => {
          this.props.startTraining(uuid);
          this._swiper.scrollBy(2, true);
          this._swiper.scrollEnabled = false;
        });
      }
      if (this.state.isProgramTrain) {
        let uuid = this.props.statuses.unfinishedProgram;
        if (!uuid) {
          this._swiper.scrollBy(1, true);
        } else {
          this.props.startProgram(uuid, true);
          this._swiper.scrollBy(2, true);
        }
        this._swiper.scrollEnabled = false;
      }
    });

  selectProgram(id) {
    UUIDGenerator.getRandomUUID().then(uuid => {
      this.props.startProgram(uuid, false, id);
      this._swiper.scrollBy(1, true);
    });
  }

  stopTraining = () => {
    this.props.stopTraining();
    this._swiper.scrollEnabled = true;
    this.setState({
      isFreeTrain: false,
      isProgramTrain: false
    });
    this._swiper.scrollBy(-2, true);
    this._swiper.scrollEnabled = false;
  };

  stopProgram = () => {
    this.props.stopProgram();
    this._swiper.scrollEnabled = true;
    this.setState({
      isFreeTrain: false,
      isProgramTrain: false
    });
    this._swiper.scrollBy(-2, true);
    this._swiper.scrollEnabled = false;
  };

  pauseProgram = (uuid, progress) => {
    this.props.pauseProgram(uuid, progress);
    this._swiper.scrollEnabled = true;
    this.setState({
      isFreeTrain: false,
      isProgramTrain: true
    });
    this._swiper.scrollBy(-2, true);
    this._swiper.scrollEnabled = false;
  };

  renderProgramItem = (item, index) => {
    return (
      <ListItem key={index} style={styles.item}>
        <CachedImage
          style={
            item.image && item.image.href
              ? styles.thumbnail
              : styles.thumbnailLogo
          }
          source={
            item.image && item.image.href
              ? {
                  uri: item.image.href
                }
              : logo
          }
        />
        <TouchableWithoutFeedback>
          <Grid>
            <Row alignItems="center">
              <Col
                onPress={() => {
                  this.selectProgram(item.id);
                }}
                size={5}
              >
                <Body style={{ justifyContent: "center" }}>
                  <Text style={styles.textHeader}>{item.class}</Text>
                  <Text note>{item.header}</Text>
                </Body>
              </Col>
            </Row>
          </Grid>
        </TouchableWithoutFeedback>
      </ListItem>
    );
  };

  render() {
    const { navigation, typeTrain, statuses } = this.props;

    console.log(statuses);

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={() => navigation.openDrawer()}>
                <Icon style={{ color: 'black' }} name='menu' />
              </Button>
            </Left>
            <Body>
              <Title>тренировка</Title>
            </Body>
            <Right />
          </Header>
          <Container>
            <Swiper
              ref={this.swiperRef}
              showsPagination={false}
              style={styles.wrapper}
              horizontal
              loop={false}
              scrollEnabled={false}
            >
              <View style={styles.slide1}>
                <Grid style={{ flex: 1 }}>
                  <Row
                    size={10}
                    style={{
                      justifyContent: "center",
                      alignContent: "flex-end",
                      alignItems: "flex-end",
                      paddingBottom: 140
                    }}
                  >
                    <TouchableWithoutFeedback onPress={this.startTraining}>
                      <Animatable.View ref={this.handleViewRef}>
                        <LinearGradient
                          colors={["#7546D7", "#b3c7f9", "#ef6aff", "#b3c7f9"]}
                          start={{ x: 0.0, y: 0.25 }}
                          end={{ x: 0.5, y: 1.0 }}
                          style={{
                            width: 170,
                            height: 170,
                            borderRadius: 170 / 2,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <View
                            style={{
                              width: 160,
                              height: 160,
                              borderRadius: 160 / 2,
                              backgroundColor: "white",
                              justifyContent: "center",
                              alignItems: "center"
                            }}
                          >
                            <View
                              style={{
                                width: 150,
                                height: 150,
                                borderRadius: 150 / 2,
                                backgroundColor: "#b896ff",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <Text style={{ color: "#EDEDED", fontSize: 30 }}>
                                {`Старт`.toUpperCase()}
                              </Text>
                            </View>
                          </View>
                        </LinearGradient>
                      </Animatable.View>
                    </TouchableWithoutFeedback>
                  </Row>
                  <Row
                    size={2}
                    style={{
                      alignContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      bordered
                      dark
                      style={
                        this.state.isFreeTrain
                          ? styles.activeButton
                          : styles.inactiveButton
                      }
                      onPress={() => this._chooseTypeTrain("free")}
                    >
                      <Text style={{ display: "flex" }}>свободная</Text>
                    </Button>

                    <Button
                      bordered
                      dark
                      style={
                        this.state.isProgramTrain
                          ? styles.activeButton
                          : styles.inactiveButton
                      }
                      onPress={() => this._chooseTypeTrain("other")}
                    >
                      <Text style={{ display: "flex" }}>программа</Text>
                    </Button>
                  </Row>
                </Grid>
              </View>
              <View style={styles.slide3}>
                <Grid style={{ flex: 1 }}>
                  <List style={{ flex: 1 }}>
                    {this.props.training.dictionaries.programs.map((p, i) =>
                      this.renderProgramItem(p, i)
                    )}
                  </List>
                </Grid>
              </View>
              <View style={styles.slide3}>
                <Grid style={{ flex: 1 }}>
                  {this.state.isProgramTrain && (
                    <Row
                      flex={1}
                      size={20}
                      style={{
                        backgroundColor: "#e1e1e1"
                      }}
                    >
                      <Button
                        onPress={this.stopProgram}
                        style={{
                          display: "flex",
                          alignContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            display: "flex",
                            textAlign: "center",
                            color: "#565656",
                            fontWeight: "600",
                            fontSize: 10
                          }}
                        >
                          Завершить программу
                        </Text>
                      </Button>
                      <Button
                        onPress={() => {
                          if (this.state.isProgramTrain) {
                            this.pauseProgram(
                              statuses.unfinishedProgram.uuid,
                              statuses.unfinishedProgram.progress
                            );
                          } else {
                            this.stopTraining();
                          }
                        }}
                        style={{
                          display: "flex",
                          alignContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            display: "flex",
                            textAlign: "center",
                            color: "#565656",
                            fontWeight: "600",
                            fontSize: 10
                          }}
                        >
                          Завершить тренировку
                        </Text>
                      </Button>
                    </Row>
                  )}
                  {!this.state.isProgramTrain && (
                    <Row
                      size={20}
                      style={{
                        backgroundColor: "#e1e1e1"
                      }}
                    >
                      <Button
                        onPress={() => {
                          if (this.state.isProgramTrain) {
                            this.pauseProgram(
                              statuses.unfinishedProgram.uuid,
                              statuses.unfinishedProgram.progress
                            );
                          } else {
                            this.stopTraining();
                          }
                        }}
                        style={{
                          flex: 1,
                          alignContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            flex: 1,
                            textAlign: "center",
                            color: "#565656",
                            fontWeight: "600"
                          }}
                        >
                          Завершить тренировку
                        </Text>
                      </Button>
                    </Row>
                  )}
                  <Row
                    size={60}
                    style={{
                      backgroundColor: "#e1e1e1"
                    }}
                  >
                    <Monitor
                      {...this.props}
                      typeTrain={this.state.isFreeTrain ? "free" : "program"}
                    />
                  </Row>
                  <Recorder {...this.props} />
                </Grid>
              </View>
            </Swiper>
          </Container>
        </Container>
      </StyleProvider>
    );
  }
}

export default connect(
  state => selector(state),
  {
    toggleRecord,
    stopRecord,
    swapDirection,
    selectTrain,
    sendActivity,
    startTraining,
    stopTraining,
    startProgram,
    pauseProgram,
    stopProgram
  }
)(TrainPage);

const styles = StyleSheet.create({
  activeButton: {
    display: "flex",
    backgroundColor: "#bfbdbd"
  },
  inactiveButton: {
    display: "flex"
  },
  wrapper: {},
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EDEDED"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  slide3: {
    // height: 400,
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: "white"
  },
  text: {
    color: "#7546D7",
    fontSize: 30,
    fontWeight: "bold"
  },
  item: {
    backgroundColor: "white",
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 10,
    paddingRight: 0
  },
  thumbnail: {
    width: 100,
    height: 100
  },
  thumbnailLogo: {
    width: 80,
    height: 65,
    marginTop: 10,
    marginBottom: 20,
    top: 0
  },
  headerTitle: {
    width: width - 150,
    textAlign: "left"
  },
  textHeader: {
    fontWeight: "600",
    color: "#EDEDED"
  }
});
