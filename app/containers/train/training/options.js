import React from "react";

import {
  StyleSheet,
  TouchableWithoutFeedback,
  Dimensions,
  FlatList
} from "react-native";

import {
  StyleProvider,
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Input,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Row,
  Grid,
  Col
} from "native-base";

import FastImage from 'react-native-fast-image'

import Utils from "../../../utils";

import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import logo from "../../../../assets/FitnessTruck.png";

const { height, width } = Dimensions.get("window");

export default class OptionsPage extends React.Component {
  constructor(props) {
    super(props);
    let params = this.props.navigation.state.params;

    let inputDataFlat;
    let inputData;
    if (params.typeTrain == "program") {
      inputData = params.dictionaries.programs.find(
        s => s.id == params.statuses.unfinishedProgram.progress.programID
      );

      inputDataFlat = inputData.programs.reduce((a, p, i) => {
        let trains =
          p.trains && p.trains.length > 0
            ? p.trains.reduce((aa, t, ii) => {
                let isPassed = false;
                let isBlocked = true;

                if (
                  params.statuses.unfinishedProgram.progress.currentBlock >=
                    i &&
                  params.statuses.unfinishedProgram.progress.currentTrain >= ii
                ) {
                  isPassed = true;
                }

                if (
                  params.statuses.unfinishedProgram.progress.currentBlock == i
                ) {
                  if (
                    params.statuses.unfinishedProgram.progress.currentTrain ==
                    ii - 1
                  ) {
                    if (!isPassed) isBlocked = false;
                  }
                }

                if (
                  params.statuses.unfinishedProgram.progress.currentBlock ==
                    -1 &&
                  params.statuses.unfinishedProgram.progress.currentTrain ==
                    -1 &&
                  i == 0 &&
                  ii == 0
                ) {
                  if (!isPassed) isBlocked = false;
                }

                if (
                  params.statuses.unfinishedProgram.progress.currentBlock !=
                    -1 &&
                  params.statuses.unfinishedProgram.progress.currentBlock ==
                    i - 1 &&
                  params.statuses.unfinishedProgram.progress.currentTrain ==
                    p.trains.length - 1 &&
                  ii == 0
                ) {
                  isBlocked = false;
                }

                if (t) {
                  aa.push({
                    ...t,
                    isPassed,
                    isBlocked,
                    isProgram: true,
                    block: i,
                    train: ii,
                    programID: inputData.id
                  });
                }

                return aa;
              }, [])
            : [];

        trains.unshift({ type: "separator", name: p.name, block: i });
        a = a.concat(trains);
        return a;
      }, []);
    } else {
      inputDataFlat = params.dictionaries.train;
    }

    this.state = {
      data: undefined,
      dataBackup: inputDataFlat
    };
    this._renderItem = this._renderItem.bind(this);
  }

  setSearchText(event) {
    searchText = event.nativeEvent.text;
    let data = this.state.dataBackup;
    searchText = searchText.trim().toLowerCase();
    data = data.filter(l => {
      return l.header ? l.header.toLowerCase().match(searchText) : false;
    });
    this.setState({
      data: data
    });
  }

  _renderItem(item) {
    if (item.type == "separator") {
      return (
        <ListItem
          style={styles.divider}
          itemDivider
          key={item.name}
          style={styles.item}
        >
          <Text style={styles.textDivider}>{item.name}</Text>
        </ListItem>
      );
    } else {
      return (
        <ListItem key={item.id} style={styles.item}>
          <FastImage
            style={
              item.img && item.img.maleImg[0]
                ? styles.thumbnail
                : styles.thumbnailLogo
            }
            source={
              item.img && item.img.maleImg[0]
                ? {
                    uri: item.img.maleImg[0]
                  }
                : logo
            }
          />
          <TouchableWithoutFeedback
            onPress={() => {
              if (!item.isBlocked && !item.isPassed) {
                this.props.navigation.state.params.selectTrain(item);
                this.props.navigation.goBack();
              }
            }}
          >
            <Grid>
              <Row alignItems="center">
                <Col
                  size={5}
                  style={
                    item.isProgram && (item.isPassed || item.isBlocked)
                      ? null
                      : styles.active
                  }
                >
                  <Body style={{ justifyContent: "center" }}>
                    <Text
                      style={
                        item.isProgram && (item.isPassed || item.isBlocked)
                          ? styles.textHeader
                          : styles.textHeaderActive
                      }
                    >
                      {item.muscleGroup}
                    </Text>
                    <Text
                      style={
                        item.isProgram && (item.isPassed || item.isBlocked)
                          ? styles.note
                          : styles.noteActive
                      }
                    >
                      {item.header}
                    </Text>
                  </Body>
                </Col>
              </Row>
            </Grid>
          </TouchableWithoutFeedback>
        </ListItem>
      );
    }
  }

  render() {
    const {
      trainOptions,
      dictionaries,
      selectTrain,
      typeTrain,
      statuses
    } = this.props.navigation.state.params;

    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="arrow-round-back" />
              </Button>
            </Left>
            <Body>
              <Text>Выбор</Text>
            </Body>
            <Right>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="add" />
              </Button>
            </Right>
          </Header>
          <InputGroup>
            <Icon name="ios-search" />
            <Input
              placeholder="Поиск"
              onChange={this.setSearchText.bind(this)}
            />
          </InputGroup>
          <Grid>
            <Row size={75}>
              <Content>
                <List
                  dataArray={this.state.data || this.state.dataBackup}
                  renderRow={this._renderItem}
                />
              </Content>
            </Row>
          </Grid>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white"
  },
  item: {
    backgroundColor: "white",
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 10,
    paddingRight: 0
  },
  right: {
    display: "flex",
    backgroundColor: "white",
    alignItems: "center",
    padding: 0,
    margin: 0
  },
  thumbnail: {
    width: 100,
    height: 100
  },
  thumbnailLogo: {
    width: 80,
    height: 65,
    marginTop: 10,
    marginBottom: 20,
    top: 0
  },
  headerTitle: {
    width: width - 150,
    textAlign: "left"
  },
  textHeader: {
    fontWeight: "600",
    color: "#565656"
  },
  textHeaderActive: {
    fontWeight: "600",
    color: "#565656"
  },
  cell: {
    alignItems: "center"
  },
  active: {
    backgroundColor: "#EDEDED"
  },
  divider: {},
  textDivider: {
    color: "#565656",
    fontSize: 16,
    fontWeight: "600"
  },
  note: {
    color: "gray"
  },
  noteActive: {
    color: "#565656"
  }
});
