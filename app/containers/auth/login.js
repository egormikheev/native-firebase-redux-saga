import React from 'react'
import { StyleSheet, Image } from 'react-native'
import { Grid, Row, Text, View, Icon, Button, Container } from 'native-base'
import { RkButton, RkAvoidKeyboard, RkTextInput } from 'react-native-ui-kitten'
import LinearGradient from 'react-native-linear-gradient'
import DropdownAlert from 'react-native-dropdownalert'
import isEmail from 'validator/lib/isEmail'
import isMatch from 'validator/lib/matches'
import firebase from 'react-native-firebase'
import Utils from '../../utils'

import logo from '../../../assets/FitnessTruck.png'

export default class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      errorMessage: null
    }
  }

  erEmPass = 'Incorrect email or password ( must contain string or numbers and have lengh more than 8 characters)'

  handleLogin = () => {
    if (
      !isEmail(this.state.email) &&
      !isMatch(this.state.password, /[a-z0-9_]{8,20}/i)
    ) {
      this.dropdown.alertWithType('error', 'Error', new String(this.erEmPass))
    } else {
      firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => this.props.navigation.navigate('Authorized'))
        .catch(error => {
          this.dropdown.alertWithType('error', 'Error', new String(error))
        })
    }
  }

  render () {
    return (
      <LinearGradient
        colors={['#EDEDED', '#EDEDED']}
        start={{ x: 0.0, y: 0.25 }}
        end={{ x: 0.5, y: 1.0 }}
        style={styles.container}
      >
        <DropdownAlert ref={ref => (this.dropdown = ref)} />
        <Image source={logo} style={styles.logo} />
        {/* <RkAvoidKeyboard> */}
          <RkTextInput
            labelStyle={{ paddingLeft: 10 }}
            rkType='rounded'
            // label='Email'
            label={
              <Icon type='Entypo' name={'email'} style={styles.inputIcon} />
            }
            style={styles.textInput}
            inputStyle={styles.inputText}
            onChangeText={email => { 
              this.setState({ email: email.replace(/\s/g, '') })}
            }
            value={this.state.email}
          />
          <RkTextInput
            labelStyle={{ paddingLeft: 10 }}
            rkType='rounded'
            label={
              <Icon
                type='MaterialIcons'
                name={'lock'}
                style={styles.inputIcon}
              />
            }
            style={styles.textInput}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
            secureTextEntry
            inputStyle={styles.inputText}
          />
        {/* </RkAvoidKeyboard> */}
        <View
          style={{
            marginTop: 16,
            flexDirection: 'row',
            flexWrap: 'wrap'
          }}
        >
          <Button bordered onPress={this.handleLogin}>
            <Icon name='md-person-add' />
            <Text>Login</Text>
          </Button>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate('SignUp')}
          >
            <Text>Sign Up</Text>
          </Button>
        </View>
      </LinearGradient>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    backgroundColor: '#EDEDED',
    width: '80%',
    borderColor: '#EDEDED',
    borderWidth: 0
  },
  inputIcon: { fontSize: 24, color: '#BB63D4' },
  inputText: {
    color: '#4962D7',
    fontSize: 18,
    fontWeight: '600'
  },
  logo: {
    height: 122,
    width: 150,
    marginBottom: 20
  }
})
