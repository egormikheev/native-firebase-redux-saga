// Loading.js
import React, { Component } from "react";
import Utils from "../../utils";
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  StyleSheet,
  Animated,
  Easing,
  AsyncStorage
} from "react-native";

import { Grid, Row } from "native-base";
import { DotIndicator } from "react-native-indicators";

import firebase from "react-native-firebase";
import LinearGradient from "react-native-linear-gradient";

import {
  initLoadingRequest,
  updateUserInfoSuccess
} from "../../redux/actions/initLoad";
import { connect } from "react-redux";
import selector from "../../redux/selectors/initLoad";

import logo from "../../../assets/FitnessTruck.png";

// TODO: добавить в секции загрузку словарей

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusMessage: "Инициализация...",
      statuses: { initLoad: false }
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.rehydrated) {
      return true;
    }
    return false;
  }

  componentWillMount() {
    if (this.props && this.props.user && this.props.user.uid) {
      this.props.navigation.navigate(this.props.user ? "Authorized" : "Login");
    }
  }

  componentDidUpdate() {
    firebase.auth().onAuthStateChanged(user => {
      if (!this.props.initLoad) {
        this.props.initLoadingRequest(user);
      } else {
        this.props.updateUserInfoSuccess(user);
        this.props.navigation.navigate(user ? "Authorized" : "Login");
      }
    });
  }

  render() {
    return (
      <LinearGradient
        colors={["#ffffff", "#ffffff"]}
        start={{ x: 0.0, y: 0.25 }}
        end={{ x: 0.5, y: 1.0 }}
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Grid>
          <Row
            size={8}
            style={{ justifyContent: "center", alignItems: "flex-end" }}
          >
            <Image
              source={logo}
              style={{
                height: 139,
                width: 170
              }}
            />
          </Row>
          <Row
            size={4}
            style={{ justifyContent: "center", alignItems: "flex-end" }}
          >
            <Text>{this.state.statusMessage}</Text>
          </Row>
          <Row
            size={3}
            style={{ justifyContent: "center", alignItems: "center" }}
          >
            <DotIndicator size={14} color={"#a3a3a3"} />
          </Row>
        </Grid>
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  state => selector(state),
  {
    initLoadingRequest,
    updateUserInfoSuccess
  }
)(Loading);
