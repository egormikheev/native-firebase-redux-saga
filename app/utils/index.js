import r from "lodash";

let Reactotron;
if (__DEV__) {
  Reactotron = require("reactotron-react-native").default;
}

export function log(data) {
  if (Reactotron) {
    Reactotron.log(data);
  } else return false;
}

export function snapshotToArray(snapshot) {
  var returnArr = [];

  snapshot.forEach(childSnapshot => {
    var item = childSnapshot.val();
    item.key = childSnapshot.key;

    returnArr.push(item);
  });

  return returnArr;
}

export function locale(lang) {
  let locales = {
    ru: {
      monthNames: [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
      ],
      monthNamesShort: [
        "Ян.",
        "Фе.",
        "Мар",
        "Апр",
        "Maй",
        "Июн",
        "Июл",
        "Авг",
        "Сен",
        "Окт",
        "Ноя",
        "Дек"
      ],
      dayNames: [
        "Понедельник",
        "Втоник",
        "Среда",
        "Четверг",
        "Пятница",
        "Суббота",
        "Воскресенье"
      ],
      dayNamesShort: ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    }
  };

  return locales[lang];
}

export default class Utils {
  static snapshotToArray = snapshotToArray;
  static log = log;
  static locale = locale;
}
