import React, { Component } from 'react'
import { View, ScrollView, TouchableOpacity, StyleSheet } from 'react-native'
import { UtilStyles } from './styles'
import {
  RkText,
  RkChoiceGroup,
  RkChoice,
  RkTheme
} from 'react-native-ui-kitten'

import Utils from '../../utils'

export default class SettingsScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      options: []
    }
    this.state.options = this.props.navigation.state.params.options
  }

  render () {
    const { params } = this.props.navigation.state
    let key = Object.keys(this.state.options)[0]

    let renderOption = (option, i) => {
      return (
        <View key={i}>
          <TouchableOpacity choiceTrigger>
            <View style={styles.checkRow}>
              <RkText rkType='bold'>{option.name}</RkText>
              <RkChoice rkType='clear' />
            </View>
          </TouchableOpacity>
        </View>
      )
    }

    return (
      <ScrollView
        style={[UtilStyles.container]}
        automaticallyAdjustContentInsets
      >
        <View style={[UtilStyles.section]}>
          <RkChoiceGroup
            selectedIndex={params.option.index}
            radio
            rkType='stretch'
            onChange={index => {
              params.onChange({ [key]: this.state.options[key][index] })
              this.props.navigation.goBack()
            }}
          >
            {this.state.options[key].map(renderOption)}
          </RkChoiceGroup>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  checkRow: {
    paddingVertical: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 16,
    paddingLeft: 16,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: RkTheme.current.colors.border.base
  }
})
