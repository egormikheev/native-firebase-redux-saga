import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import Utils from '../../utils'
import { UtilStyles } from './styles'

import {
  Text,
  Icon
} from 'native-base'

import {
  RkText
} from 'react-native-ui-kitten'

const item = [
  {
    header: 'Фильтр'
  }
]

const { width } = Dimensions.get('window')

export default class FilterTrains extends Component {
  constructor (props) {
    super(props)

    this.state = {
      activeSections: [],
      collapsed: true,
      settingsOption: {
        muscleGroup: {
          name: 'Все',
          index: -1
        },
        equipment: {
          name: 'Все',
          index: -1
        },
        typeTrain: {
          name: 'Все',
          index: -1
        }
      }
    }
  }

  componentWillReceiveProps(newProps) {
    Utils.log('hey')
    Utils.log({ newProps })
    this.setState({ activeSections: newProps.filterActive ? [0]: [], settingsOption: {
      ...this.state.settingsOption,
    } })
  }

  _showSettingsScreen (options) {
    this.props.navigation.navigate('FilterOptions', {
      option: this.state.settingsOption,
      options: options,
      onChange: option => {
        this.setState({
          settingsOption: {
            ...this.state.settingsOption,
            ...option
          }
        })
      }
    })
  }

  _resetFilter (key) {
    this.setState({
      settingsOption: {
        ...this.state.settingsOption,
        [key]: {
          name: 'Все',
          index: -1
        }
      }
    })
  }

  _setSection (activeSections) {
    Utils.log(activeSections)
    if (this.state.activeSections.length === 0) {
      this.props.applyFilter(this.state.settingsOption)
    }
    this.setState({ activeSections })
  }

  _renderHeader (section, i, isActive) {
    return (
      <View
        style={[styles.headerIcon, isActive ? styles.active : styles.inactive]}
      >
        <Text style={styles.headerText}>Фильтр</Text>
        <Icon style={styles.icon} name={'sliders'} type={'Feather'} />
      </View>
    )
  }

  _renderContent () {
    const {
      muscleGroup,
      equipment,
      level,
      type,
      typeTrain
    } = this.props.userdata.dictionaries

    return (
      <View style={styles.header}>
        <View style={[UtilStyles.bordered]}>
          <TouchableOpacity
            style={{ padding: 23 }}
            onPress={() => {
              this._showSettingsScreen({
                muscleGroup
              })
            }}
          >
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <RkText>Группа мышц</RkText>
              <View style={{ flexDirection: 'row' }}>
                <RkText rkType='bold'>
                  {this.state.settingsOption.muscleGroup.name}
                </RkText>
                {this.state.settingsOption.muscleGroup.index == -1 &&
                  <Icon
                    name={'ios-arrow-dropright-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
                {this.state.settingsOption.muscleGroup.index != -1 &&
                  <Icon
                    onPress={() => this._resetFilter('muscleGroup')}
                    name={'ios-close-circle-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <View style={[UtilStyles.bordered]}>
          <TouchableOpacity
            style={{ padding: 23 }}
            onPress={() => {
              this._showSettingsScreen({
                equipment
              })
            }}
          >
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <RkText>Оборудование</RkText>
              <View style={{ flexDirection: 'row' }}>
                <RkText rkType='bold'>
                  {this.state.settingsOption.equipment.name}
                </RkText>
                {this.state.settingsOption.equipment.index == -1 &&
                  <Icon
                    name={'ios-arrow-dropright-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
                {this.state.settingsOption.equipment.index != -1 &&
                  <Icon
                    onPress={() => this._resetFilter('equipment')}
                    name={'ios-close-circle-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <View style={[UtilStyles.bordered]}>
          <TouchableOpacity
            style={{ padding: 23 }}
            onPress={() => {
              this._showSettingsScreen({
                typeTrain
              })
            }}
          >
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <RkText>Тип тренировки</RkText>
              <View style={{ flexDirection: 'row' }}>
                <RkText rkType='bold'>
                  {this.state.settingsOption.typeTrain.name}
                </RkText>
                {this.state.settingsOption.typeTrain.index == -1 &&
                  <Icon
                    name={'ios-arrow-dropright-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
                {this.state.settingsOption.typeTrain.index != -1 &&
                  <Icon
                    onPress={() => this._resetFilter('typeTrain')}
                    name={'ios-close-circle-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render () {
    return (
      <Accordion
        sections={item}
        activeSections={this.state.activeSections}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent.bind(this)}
        // renderSectionTitle={this._renderContent.bind(this)}
        touchableComponent={TouchableOpacity}
        onChange={this._setSection.bind(this)}
        settingsOption={this.state.settingsOption}
        showSettingsScreen={this._showSettingsScreen.bind(this)}
        navigation={this.props.navigation}
        dictionaries={this.props.userdata.dictionaries}
        applyFilter={this.applyFilter}
      />
    )
  }
}

const styles = StyleSheet.create({
  header: {
    paddingLeft: 20,
    // paddingTop: 10,
    // paddingBottom: 10,
    backgroundColor: '#ececec',
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    width: width
  },
  headerIcon: {
    paddingRight: 20,
    // paddingTop: 10,
    // paddingBottom: 10,
    backgroundColor: '#ececec',
    display: 'flex',
    // alignItems: "flex-end",
    flex: 1,
    flexDirection: 'row',
    width: width,
    justifyContent: 'space-between'
  },
  icon: {
    color: '#ececec',
    fontSize: 14,
    display: 'flex'
  },
  headerText: {
    fontSize: 14,
    display: 'flex',
    paddingLeft: 15
  },
  active: {
    backgroundColor: '#EDEDED',
    borderBottomColor: '#EDEDED',
    borderBottomWidth: 6
  },
  inactive: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2,
    backgroundColor: '#EDEDED'
  }
})
