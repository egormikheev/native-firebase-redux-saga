import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import Utils from '../../utils'
import { UtilStyles } from './styles'

import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Grid,
  Row,
  Icon
} from 'native-base'

import {
  RkText,
  RkChoiceGroup,
  RkChoice,
  RkTheme
} from 'react-native-ui-kitten'

const { width } = Dimensions.get('window')

const item = [
  {
    header: 'Фильтр'
  }
]

export default class FilterPrograms extends Component {
  constructor (props) {
    super(props)

    this.state = {
      activeSections: [],
      collapsed: true,
      settingsOption: {
        programClass: {
          name: 'Все',
          index: -1
        }
      }
    }
  }

  _showSettingsScreen (options) {
    this.props.navigation.navigate('FilterOptionsPrograms', {
      option: this.state.settingsOption,
      options: options,
      onChange: option => {
        this.setState({
          settingsOption: {
            ...this.state.settingsOption,
            ...option
          }
        })
      }
    })
  }

  _resetFilter (key) {
    this.setState({
      settingsOption: {
        ...this.state.settingsOption,
        [key]: {
          name: 'Все',
          index: -1
        }
      }
    })
  }

  _setSection (activeSections) {
    if (this.state.activeSections.length === 0) {
      this.props.applyFilterPrograms(this.state.settingsOption)
    }
    this.setState({ activeSections })
  }

  _renderHeader (section, i, isActive) {
    return (
      <View
        style={[styles.headerIcon, isActive ? styles.active : styles.inactive]}
      >
        <Text style={styles.headerText}>Фильтр</Text>
        <Icon style={styles.icon} name={'sliders'} type={'Feather'} />
      </View>
    )
  }

  _renderContent () {
    const { programClass } = this.props.dictionaries

    return (
      <View style={styles.header}>
        <View style={[UtilStyles.bordered]}>
          <TouchableOpacity
            style={{ paddingRight: 23, paddingBottom: 10, paddingTop: 10 }}
            onPress={() => {
              this._showSettingsScreen({
                programClass
              })
            }}
          >
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <RkText>Класс тренировок</RkText>
              <View style={{ flexDirection: 'row' }}>
                <RkText rkType='bold'>
                  {this.state.settingsOption.programClass.name}
                </RkText>
                {this.state.settingsOption.programClass.index == -1 &&
                  <Icon
                    name={'ios-arrow-dropright-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
                {this.state.settingsOption.programClass.index != -1 &&
                  <Icon
                    onPress={() => this._resetFilter('programClass')}
                    name={'ios-close-circle-outline'}
                    size={20}
                    style={{ opacity: 0.3, paddingLeft: 10 }}
                  />}
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render () {
    return (
      <ListItem
        key={0}
        style={{
          paddingRight: 0,
          paddingTop: 0,
          paddingBottom: 0,
          marginLeft: 0,
          display: 'flex',
          flex: 1
        }}
      >
        <Accordion
          sections={item}
          activeSections={this.state.activeSections}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent.bind(this)}
          // renderSectionTitle={this._renderContent.bind(this)}
          touchableComponent={TouchableOpacity}
          onChange={this._setSection.bind(this)}
          settingsOption={this.state.settingsOption}
          showSettingsScreen={this._showSettingsScreen.bind(this)}
          navigation={this.props.navigation}
          dictionaries={this.props.dictionaries}
        />
      </ListItem>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#ececec',
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    width: width
  },
  headerIcon: {
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#ececec',
    display: 'flex',
    // alignItems: "flex-end",
    flex: 1,
    flexDirection: 'row',
    width: width,
    justifyContent: 'space-between'
  },
  icon: {
    color: '#ececec',
    fontSize: 20,
    display: 'flex'
  },
  headerText: {
    fontSize: 16,
    display: 'flex',
    paddingLeft: 15
  },
  active: {
    backgroundColor: '#EDEDED',
    borderBottomColor: '#EDEDED',
    borderBottomWidth: 6
  },
  inactive: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2,
    backgroundColor: '#EDEDED'
  }
})
