import React from "react";
import { StyleSheet } from "react-native";

import {
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Grid,
  Row
} from "native-base";

import Utils from "../../utils";

import Options from "../../containers/train/training/options";

export default class TrainingList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      training,
      selectTrain,
      stopRecord,
      typeTrain,
      statuses
    } = this.props;

    let selectedTrainName =
      training.trainOptions.selectedTrain &&
      training.trainOptions.selectedTrain.id
        ? training.trainOptions.selectedTrain.header
        : "Выбрать упраженение...";

    return (
      <Content>
        <List>
          <ListItem
            onPress={() => {
              this.props.navigation.navigate("TrainOptions", {
                ...training,
                selectTrain,
                statuses,
                typeTrain
              });
              stopRecord();
            }}
          >
            <Text style={styles.text}>{selectedTrainName}</Text>
          </ListItem>
        </List>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: "#7546D7"
  }
});
