import React from "react";
import R from "reactotron-react-native";

import {
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  Toast,
  ActionSheet
} from "native-base";

import { Col, Row, Grid } from "native-base";

export default class Recorder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      swapPhase: false,
      showToast: false
    };

    this.countSwap = this.countSwap.bind(this);
  }

  componentWillUnmount() {
    this.props.stopRecord();
  }

  countSwap = () => {
    if (this.props.training.recorder.record) {
      this.props.swapDirection();
      this.setState({ swapPhase: true });
      setTimeout(() => {
        this.setState({ swapPhase: false });
      }, 100);
    }
  };

  render() {
    const {
      recorder,
      middleBtnName,
      leftBtnName,
      righBtnName,
      dictionaries,
      trainOptions
    } = this.props.training;

    const { toggleRecord, stopRecord, swapDirection } = this.props;

    return (
      <Row
        size={recorder.record || recorder.pause ? 20 : 10}
        style={{
          alignContent: "center",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#EDEDED"
        }}
      >
        <Col>
          <Button
            onPress={stopRecord}
            large
            transparent
            full
            style={{
              height: recorder.record || recorder.pause ? 110 : 50,
              padding: 0
            }}
          >
            <Icon
              name={leftBtnName}
              style={{ fontSize: 40, color: "#565656" }}
            />
          </Button>
        </Col>
        <Col>
          <Button
            onPress={() => {
              if (trainOptions.selectedTrain) {
                toggleRecord(this.props.training);
              } else {
                Toast.show({
                  text: "Выберите упражнение!",
                  buttonText: "Ок",
                  duration: 3000,
                  position: "top"
                });
              }
            }}
            large
            transparent
            full
            style={{
              height: recorder.record || recorder.pause ? 110 : 50,
              padding: 0
            }}
          >
            <Icon
              name={middleBtnName}
              style={{
                fontSize: recorder.record ? 90 : 40,
                color:
                  !recorder.record && recorder.stoped ? "#565656" : "#7546D7"
              }}
            />
          </Button>
        </Col>
        <Col>
          <Button
            onPress={this.countSwap}
            large
            full
            transparent
            style={{
              height: recorder.record || recorder.pause ? 110 : 50,
              padding: 0
            }}
          >
            <Icon
              name="swap"
              style={{
                fontSize: 40,
                color: !this.state.swapPhase ? "#565656" : "#7546D7"
              }}
            />
          </Button>
        </Col>
      </Row>
    );
  }
}
