import React from "react";
import m from "moment";
import f from "moment-duration-format";
import { StyleSheet } from "react-native";
import _ from "lodash";

f(m);
import BackgroundTimer from "react-native-background-timer";
import TrainingList from "../../components/training/TrainingList";
import RNFS from 'react-native-fs'

import Utils from "../../utils";
const path = RNFS.ExternalStorageDirectoryPath + `/Documents/${m().format('YYYYMMDD')}dataactivities.txt`

import {
  Container,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Input,
  Item,
  Label,
  Button,
  Text,
  List,
  ListItem,
  InputGroup,
  Grid,
  Row,
  Col,
  Badge
} from "native-base";

export default class Monitor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      startTime: 0,
      pausedTime: 0,
      currentDuration: "00:00:00",
      totalDuration: 0,
      ticks: [],
      status: "stoped",
      swapsCount: 0,
      ticksTime: 0,
      startNewTick: 0
    };

    this.timerRef = null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.timers.trainTimer.status != prevProps.timers.trainTimer.status
    ) {
      switch (this.props.timers.trainTimer.status) {
        case "started":
          if (prevState.status == "stoped" || prevState.status == "paused") {
            let date = new Date().getTime();
            let startTime =
              prevState.status == "paused"
                ? this.state.startTime
                : new Date().getTime();
            let totalDuration = 0;

            if (this.state.pausedTime) {
              totalDuration =
                this.state.totalDuration + (date - this.state.pausedTime);
            }

            this.timerRef = BackgroundTimer.setInterval(() => {
              const currentDuration =
                new Date().getTime() - startTime - totalDuration;

              if (prevState.status == "paused") {
                this.setState({
                  currentDuration,
                  totalDuration,
                  startNewTick: date,
                  status: "started"
                });
              } else {
                this.setState({
                  currentDuration,
                  startTime: date,
                  status: "started"
                });
              }
            }, 10);
          }
          return;
        case "paused":
        case "stoped":
        default:
          BackgroundTimer.clearInterval(this.timerRef);
          let ticks = this.state.ticks;
          let ticksTime =
            ticks.length > 0
              ? ticks.map(x => x.duration).reduce((s, n) => s + n)
              : 0;
          let startTick =
            this.state.startTime + this.state.totalDuration + ticksTime;
          let stopTick = new Date().getTime();
          let duration = stopTick - startTick;

          let swaps;
          if (this.state.swapsCount > 0) {
            swaps = this.props.training.recorder.swaps
              ? _.cloneDeep(
                  this.props.training.recorder.swaps.slice(
                    this.state.swapsCount
                  )
                )
              : null;
          } else {
            swaps = this.props.training.recorder.swaps
              ? _.cloneDeep(this.props.training.recorder.swaps)
              : null;
          }

          let swapsCount = swaps
            ? swaps.length + this.state.swapsCount
            : this.state.swapsCount
              ? this.state.swapsCount
              : 0;

          ticks.push({ startTick, stopTick, duration, swaps });

          if (this.props.timers.trainTimer.status == "stoped") {
            if (
              prevProps.training.trainOptions.selectedTrain &&
              ticks &&
              ticks.length > 0
            ) {
              Utils.log({ PROPSTRAIN: prevProps });

              let trainingId = this.props.training.trainingId
                ? this.props.training.trainingId
                : prevProps.statuses.unfinishedProgram.uuid;

              let dateStop = new Date().getTime();
              const filedata = JSON.stringify({
                activity: prevProps.training.trainOptions.selectedTrain,
                ticks: ticks,
                dateStart: this.state.startTime,
                dateStop: dateStop,
                inactivityTime: this.state.totalDuration,
                activityTime: this.state.currentDuration,
                swapsCount: swapsCount,
                ticksCount: ticks.length,
                formatDate: m(dateStop).format("YYYY-MM-DD"),
                trainingId: trainingId,
                isProgram: prevProps.training.trainOptions.selectedTrain
                  .isProgram
                  ? true
                  : false
              }, null, 2)

            RNFS.appendFile(path, filedata).then(() => {
              this.props.sendActivity({
                activity: prevProps.training.trainOptions.selectedTrain,
                ticks: ticks,
                dateStart: this.state.startTime,
                dateStop: dateStop,
                inactivityTime: this.state.totalDuration,
                activityTime: this.state.currentDuration,
                swapsCount: swapsCount,
                ticksCount: ticks.length,
                formatDate: m(dateStop).format("YYYY-MM-DD"),
                trainingId: trainingId,
                isProgram: prevProps.training.trainOptions.selectedTrain
                  .isProgram
                  ? true
                  : false
              });
              this.setState({
                currentDuration: "00:00:00",
                status: "stoped",
                pausedTime: 0,
                totalDuration: 0,
                ticks: [],
                swapsCount: 0,
                startTime: 0,
                startNewTick: 0
              });
            })
            }
          } else {
            this.setState({
              status: "paused",
              ticks,
              pausedTime: stopTick,
              ticksTime,
              swapsCount
            });
          }
      }
    }
  }

  renderTicks(ticks) {
    let rendered;
    if (ticks && ticks.length > 0) {
      rendered = ticks.map((t, i) => (
        <Text key={i} style={styles.text}>
          {m.duration(t.duration, "milliseconds").format("mm:ss:SS", {
            trim: false
          })}
        </Text>
      ));
    }
    return (
      <Container>
        {rendered}
        <Text style={styles.text}>
          {m
            .duration(
              this.state.startNewTick != 0
                ? new Date().getTime() - this.state.startNewTick
                : this.state.currentDuration,
              "milliseconds"
            )
            .format("mm:ss:SS", {
              trim: false
            })}
        </Text>
      </Container>
    );
  }

  renderActivityInfo() {
    return (
      <Row>
        <Col
          size={1}
          style={{
            marginLeft: 15
          }}
        >
          <Icon type="Entypo" name="area-graph" style={{ color: "#565656" }} />
        </Col>
        <Col size={3}>
          <Text style={styles.textLabel}>
            {this.state.ticks.length ? this.state.ticks.length : "подходов"}
          </Text>
        </Col>
        <Col size={1}>
          <Icon
            type="Entypo"
            name="circular-graph"
            style={{ color: "#565656", paddingLeft: 2 }}
          />
        </Col>
        <Col
          size={4}
          style={{
            marginRight: 15
          }}
        >
          <Row>
            {this.state.ticks && this.state.ticks.length > 0 ? (
              this.state.ticks.map((t, i) => (
                <Text key={i} style={styles.textLabel}>
                  {t.swaps && t.swaps.length} {` `}
                </Text>
              ))
            ) : (
              <Text style={styles.textLabel}>повторений</Text>
            )}
          </Row>
        </Col>
      </Row>
    );
  }

  render() {
    const { status, startTime, ticks } = this.props.timers.trainTimer;

    return (
      <Grid>
        <Row size={50}>
          <Col
            size={70}
            style={{
              alignContent: "center"
            }}
          >
            <Text style={styles.textClock}>
              {m
                .duration(this.state.currentDuration, "milliseconds")
                .format("mm:ss:SS", {
                  trim: false
                })}
            </Text>
            <TrainingList {...this.props} />
          </Col>
          <Col size={30}>
            <Content style={{ paddingTop: 10 }}>
              {this.renderTicks(this.state.ticks)}
            </Content>
          </Col>
        </Row>
        <Row size={30}>{this.renderActivityInfo()}</Row>
        <Row size={20} />
      </Grid>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: "#7546D7"
  },
  textLabel: {
    color: "#565656",
    paddingTop: 4,
    fontSize: 16
  },
  textClock: {
    color: "#7546D7",
    fontSize: 46,
    marginLeft: 15,
    fontWeight: "500"
  }
});
