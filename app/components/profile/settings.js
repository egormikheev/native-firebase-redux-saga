import React, { Component } from 'react'
import { View, ScrollView, StyleSheet } from 'react-native'
import { UtilStyles } from './styles'
import { RkTheme } from 'react-native-ui-kitten'

import { Grid, Row, Col, Text, Button } from 'native-base'

import {
  getAllSensorData,
  clearAllSensorData
} from '../../redux/actions/userdata'

import selector from '../../redux/selectors/settings'

import { connect } from 'react-redux'

import Utils from '../../utils'

class SettingsScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      options: []
    }

    this.getAllSensorData = this.props.getAllSensorData.bind(this)
    this.clearAllSensorData = this.props.clearAllSensorData.bind(this)
  }

  render () {
    const { settings: { downloadCSV, clearData } } = this.props
   
    return (
      <ScrollView
        style={[UtilStyles.container]}
        automaticallyAdjustContentInsets
      >
        <View style={[UtilStyles.section]}>
          <Grid>
            <Row style={styles.row}>
              <Col size={5}>
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 13,
                    paddingBottom: 5,
                    paddingTop: 5
                  }}
                >
                  Скачать данные активностей на диск устройства
                </Text>
              </Col>
              <Col size={3}>
                <Button 
                  full 
                  disabled={downloadCSV.pending} 
                  onPress={() => this.getAllSensorData()}
                >
                  <Text>Сохранить</Text>
                </Button>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col size={5}>
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 13,
                    paddingBottom: 5,
                    paddingTop: 5
                  }}
                >
                  Сбросить все данные об активностях на сервере
                </Text>
              </Col>
              <Col size={3}>
                <Button 
                  full 
                  disabled={clearData.pending || downloadCSV.pending} 
                  onPress={() => this.clearAllSensorData()}
                >
                  <Text>Сбросить</Text>
                </Button>
              </Col>
            </Row>
          </Grid>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  checkRow: {
    paddingVertical: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 16,
    paddingLeft: 16,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: RkTheme.current.colors.border.base
  },
  row: {
    paddingBottom: 10
  }
})

export default connect(state => selector(state), 
{ 
  getAllSensorData, 
  clearAllSensorData 
})( SettingsScreen )
