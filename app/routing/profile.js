import React from 'react'
import History from '../containers/profile/history'
import User from '../containers/profile/user'
import Settings from '../containers/profile/settings'

import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from 'react-navigation'
import { StyleSheet } from 'react-native'
import { Footer, StyleProvider } from 'native-base'
import getTheme from '../../native-base-theme/components'
import material from '../../native-base-theme/variables/material'

export default createMaterialTopTabNavigator(
  {
    main: { screen: User },
    train: { screen: History },
    settings: { screen: Settings }
  },
  {
    tabBarPosition: 'bottom',
    tabBarComponent: props => {
      const styles = StyleSheet.create({
        icon: {
          color: 'black'
        },
        bigIcon: {
          fontSize: 45,
          color: 'black'
        }
      })

      return (
        <StyleProvider style={getTheme(material)}>
          <Footer hide style={{ height: 0 }} />
        </StyleProvider>
      )
    }
  }
)
