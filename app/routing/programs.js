import React from 'react'
import Trains from '../containers/program/trains'
import FilterOptions from '../containers/program/trains/options'
import TrainScreen from '../containers/program/trains/trainInfo'
import ProgramScreen from '../containers/program/programs/programinfo'

import AddTrain from '../containers/program/trains/add'

import Programs from '../containers/program/programs'
import Favorites from '../containers/program/favorites'
import { Footer, StyleProvider } from 'native-base'
import getTheme from '../../native-base-theme/components'
import material from '../../native-base-theme/variables/material'

import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from 'react-navigation'
import { StyleSheet } from 'react-native'

const stackNav = createStackNavigator(
  {
    TrainsPage: {
      screen: Trains
    },
    FilterOptions: {
      screen: FilterOptions
    },
    TrainScreen: {
      screen: TrainScreen
    },
    AddTrain: {
      screen: AddTrain
    }
  },
  {
    headerMode: 'none'
  }
)

const stackNav2 = createStackNavigator(
  {
    ProgramsPage: {
      screen: Programs
    },
    FilterOptionsPrograms: {
      screen: FilterOptions
    },
    ProgramScreen: {
      screen: ProgramScreen
    },
    ProgramTrainScreen: {
      screen: TrainScreen
    },
    AddProgram: {
      screen: AddTrain
    }
  },
  {
    headerMode: 'none'
  }
)

export default createMaterialTopTabNavigator(
  {
    trains: { screen: stackNav },
    programs: { screen: stackNav2 },
    favorites: { screen: Favorites }
  },
  {
    tabBarPosition: 'bottom',
    tabBarComponent: props => {
      const styles = StyleSheet.create({
        icon: {
          color: 'black'
        },
        bigIcon: {
          fontSize: 45,
          color: 'black'
        }
      })

      return (
        <StyleProvider style={getTheme(material)}>
          <Footer hide style={{ height: 0 }} />
        </StyleProvider>
      )
    }
  }
)
