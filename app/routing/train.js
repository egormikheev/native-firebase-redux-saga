import React from 'react'
import MainPage from '../containers/train/main'
import TrainPage from '../containers/train/training'
import TrainOptions from '../containers/train/training/options'
import SettingsPage from '../containers/train/settings'

import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from 'react-navigation'
import { StyleSheet } from 'react-native'
import { Footer, StyleProvider } from 'native-base'

import getTheme from '../../native-base-theme/components'
import material from '../../native-base-theme/variables/material'

// навигатор по страницам выбора опций записи тренировки
const stackNav = createStackNavigator(
  {
    TrainPage: {
      screen: TrainPage
    },
    TrainOptions: {
      screen: TrainOptions
    }
  },
  {
    // mode: 'modal',
    headerMode: 'none'
  }
)

export default createMaterialTopTabNavigator(
  {
    main: { screen: MainPage },
    train: { screen: stackNav },
    settings: { screen: SettingsPage }
  },
  {
    tabBarPosition: 'bottom',
    tabBarComponent: props => {
      const styles = StyleSheet.create({
        icon: {
          color: 'black'
        },
        bigIcon: {
          fontSize: 45,
          color: 'black'
        }
      })

      return (
        <StyleProvider style={getTheme(material)}>
          <Footer hide style={{ height: 0 }} />
        </StyleProvider>
      )
    }
  }
)
