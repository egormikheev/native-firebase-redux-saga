export const START_TIMER = 'TRAINING/START_TIMER'
export const STOP_TIMER = 'TRAINING/STOP_TIMER'
export const RESET_TIMER = 'TRAINING/RESET_TIMER'

export const SEND_ACTIVITY = 'TRAINING/SEND_ACTIVITY'

export function startTimer () {
  return {
    type: START_TIMER
  }
}

export function stopTimer () {
  // pause!!!
  return {
    type: STOP_TIMER
  }
}

export function resetTimer () {
  return {
    type: RESET_TIMER
  }
}

export function sendActivity (activity) {
  return {
    type: SEND_ACTIVITY,
    payload: activity,
    meta: {
      offline: {
        // the network action to execute:
        effect: {
          url: `https://bigdog-7b6c2.firebaseio.com/store/actions.json`,
          method: 'POST',
          body: JSON.stringify(activity)
        }
        // action to dispatch when effect succeeds:
        // commit: { type: 'FOLLOW_USER_COMMIT', meta: { userId } },
        // // action to dispatch if network action fails permanently:
        // rollback: { type: 'FOLLOW_USER_ROLLBACK', meta: { userId } }
      }
    }
  }
}
