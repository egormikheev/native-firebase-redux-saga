import Utils from "../../utils";

export const SET_PERIPHERALS = "BLUETOOTH/SET_PERIPHERALS";

export function setPeripherals(items) {
  Utils.log({ "SET PERIPHPERALS": items });
  return {
    type: SET_PERIPHERALS,
    payload: items
  };
}
