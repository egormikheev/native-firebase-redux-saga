export const REHYDRATE_STORE = "REHYDRATE_STORE";
import Utils from "../../utils";

export function rehydrateStore() {
  return { type: REHYDRATE_STORE };
}
