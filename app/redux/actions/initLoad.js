export const INITIAL_LOADING_REQUEST = "INIT/INITIAL_LOADING_REQUEST";
export const INITIAL_LOADING_SUCCESS = "INIT/INITIAL_LOADING_SUCCESS";
export const INITIAL_LOADING_FAILURE = "INIT/INITIAL_LOADING_FAILURE";

export const UPDATE_USER_INFO_REQUEST = "INIT/UPDATE_USER_INFO_REQUEST";
export const UPDATE_USER_INFO_SUCCESS = "INIT/UPDATE_USER_INFO_SUCCESS";

export function initLoadingRequest(userId) {
  return {
    type: INITIAL_LOADING_REQUEST,
    payload: userId
  };
}

export function initLoadingSuccess(payload) {
  return {
    type: INITIAL_LOADING_SUCCESS,
    payload
  };
}

export function initLoadingFailure(payload) {
  return {
    type: INITIAL_LOADING_FAILURE,
    payload
  };
}

export function updateUserInfoSuccess(user) {
  return {
    type: UPDATE_USER_INFO_SUCCESS,
    payload: user
  };
}
