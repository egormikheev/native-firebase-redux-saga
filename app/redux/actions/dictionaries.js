export const RECIEVE_TRAINING_ACTIONS = 'RECIEVE_TRAINING_ACTIONS'

export function recieveTrainingActions (actions) {
  return {
    type: RECIEVE_TRAINING_ACTIONS,
    payload: actions
  }
}
