export const GET_CALENDAR_DAYS_REQUEST = 'CALENDAR/GET_CALENDAR_DAYS_REQUEST'
export const GET_CALENDAR_DAYS_SUCCESS = 'CALENDAR/GET_CALENDAR_DAYS_SUCCESS'

export function getCalendarDays (days) {
  return {
    type: GET_CALENDAR_DAYS_REQUEST,
    payload: days
  }
}

export function getCalendarDaysSuccess (days, mode) {
  return {
    type: GET_CALENDAR_DAYS_SUCCESS,
    payload: { days, mode }
  }
}
