import { createRoutine } from 'redux-saga-routines'

export const RECIEVE_USER_ACTIVITY = 'USER_DATA/RECIEVE_USER_ACTIVITY'
export const GET_MORE_TRAININGS = 'USER_DATA/GET_MORE_TRAININGS'
export const APPLY_FILTER = 'USER_DATA/APPLY_FILTER'
export const TOGGLE_TRAIN_TO_FAVORITES = 'USER_DATA/TOGGLE_TRAIN_TO_FAVORITES'
export const ADD_TRAIN_REQUEST = 'USER_DATA/ADD_TRAIN_REQUEST'
export const ADD_TRAIN_SUCCESS = 'USER_DATA/ADD_TRAIN_SUCCESS'

export const GET_MORE_PROGRAMS = 'USER_DATA/GET_MORE_PROGRAMS'
export const APPLY_FILTER_PROGRAMS = 'USER_DATA/APPLY_FILTER_PROGRAMS'
export const TOGGLE_PROGRAM_TO_FAVORITES =
  'USER_DATA/TOGGLE_PROGRAM_TO_FAVORITES'
export const ADD_PROGRAM_REQUEST = 'USER_DATA/ADD_PROGRAM_REQUEST'
export const ADD_PROGRAM_SUCCESS = 'USER_DATA/ADD_PROGRAM_SUCCESS'

export const getAllSensorData = createRoutine('GET_ALL_SENSOR_DATA')
export const clearAllSensorData = createRoutine('CLEAR_ALL_SENSOR_DATA')

import Utils from '../../utils'

export function recieveUserActivity (actions) {
  return {
    type: RECIEVE_USER_ACTIVITY,
    payload: actions
  }
}

export function getMoreTrainings () {
  return { type: GET_MORE_TRAININGS }
}

export function applyFilter (filter) {
  return { type: APPLY_FILTER, payload: filter }
}

export function toggleTrainToFavorites (id, isFavorite) {
  return { type: TOGGLE_TRAIN_TO_FAVORITES, payload: { id, isFavorite } }
}

export function setNewTrain (train) {
  return {
    type: ADD_TRAIN_REQUEST,
    payload: train
  }
}

export function getMorePrograms () {
  return { type: GET_MORE_PROGRAMS }
}

export function applyFilterPrograms (filter) {
  return { type: APPLY_FILTER_PROGRAMS, payload: filter }
}

export function toggleProgramToFavorites (id, isFavorite) {
  return { type: TOGGLE_PROGRAM_TO_FAVORITES, payload: { id, isFavorite } }
}

export function setNewProgram (program) {
  return {
    type: ADD_PROGRAM_REQUEST,
    payload: program
  }
}
