import R from "reactotron-react-native";

export const TOGGLE_PLAY = "TRAINING/TOGGLE_RECORD";
export const STOP = "TRAINING/STOP_REC";
export const SWAP_DIRECTION = "TRAINING/SWAP_DIRECTION";
export const SELECT_TRAIN = "TRAINING/SELECT_TRAIN";

export const START_TRAINING = "TRAINING/START_TRAINING";
export const STOP_TRAINING = "TRAINIG/STOP_TRAINING";

export const START_PROGRAM = "TRAINING/START_PROGRAM";
export const PAUSE_PROGRAM = "TRAINING/PAUSE_PROGRAM";
export const STOP_PROGRAM = "TRAINIG/STOP_PROGRAM";

export const SEND_ACCELEROMETER_DATA = "TRAINING/SEND_ACCELEROMETER_DATA";

export function sendAccelerometerData(data) {
    return {
      type: SEND_ACCELEROMETER_DATA,
      payload: data,
      meta: {
        offline: {
          // the network action to execute:
          effect: {
            url: `https://bigdog-7b6c2.firebaseio.com/store/sensorsdata.json`,
            method: "POST",
            body: JSON.stringify(data)
          }
          // action to dispatch when effect succeeds:
          // commit: { type: 'FOLLOW_USER_COMMIT', meta: { userId } },
          // // action to dispatch if network action fails permanently:
          // rollback: { type: 'FOLLOW_USER_ROLLBACK', meta: { userId } }
        }
      }
    };
}

export function startTraining(uuid) {
  return {
    type: START_TRAINING,
    payload: uuid
  };
}

export function startProgram(uuid, isContinue, id) {
  return {
    type: START_PROGRAM,
    payload: {
      uuid,
      isContinue,
      id
    }
  };
}

export function stopProgram() {
  return {
    type: STOP_PROGRAM
  };
}

export function pauseProgram(uuid, progress) {
  return {
    type: PAUSE_PROGRAM,
    payload: {
      uuid,
      progress
    }
  };
}

export function stopTraining() {
  return {
    type: STOP_TRAINING
  };
}

export function toggleRecord(state) {
  return {
    type: TOGGLE_PLAY,
    payload: state
  };
}

export function stopRecord() {
  return {
    type: STOP
  };
}

export function swapDirection() {
  return {
    type: SWAP_DIRECTION
  };
}

export function selectTrain(id) {
  return {
    type: SELECT_TRAIN,
    payload: id
  };
}