import m from "moment";
import r from "reactotron-react-native";
import l from "lodash";

import {
  START_TIMER,
  STOP_TIMER,
  RESET_TIMER,
  SEND_ACTIVITY
} from "../actions/timer";

let initialState = {
  trainTimer: {
    status: "stoped"
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case START_TIMER:
      return {
        ...state,
        trainTimer: {
          status: "started"
        }
      };
    case STOP_TIMER:
      let newTicks = state.trainTimer.ticks;

      return {
        ...state,
        trainTimer: {
          status: "paused"
        }
      };

    case RESET_TIMER:
      return {
        ...state,
        trainTimer: {
          status: "stoped"
        }
      };
    default:
      return state;
  }
}
