import { combineReducers } from 'redux'
import Utils from '../../utils'

import training from './training'
import timers from './timer'
import userdata from './userdata'
import calendar from './calendar'
import session from './session'

const allReducers = combineReducers({
  training,
  timers,
  userdata,
  session,
  calendar
})

export default function root (state, action) {
  return allReducers(state, action)
}
