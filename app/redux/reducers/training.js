import m from "moment";

import {
  TOGGLE_PLAY,
  STOP,
  SWAP_DIRECTION,
  SELECT_TRAIN,
  START_TRAINING,
  STOP_TRAINING,
  START_PROGRAM,
  PAUSE_PROGRAM
} from "../actions/training";
import { RECIEVE_TRAINING_ACTIONS } from "../actions/dictionaries";

import R from "reactotron-react-native";

let initialState = {
  recorder: {
    record: false,
    stoped: true,
    pause: false,
    swaps: []
  },
  middleBtnName: "radio-button-off",
  leftBtnName: "medical",
  rigthBtnName: "swap",
  trainOptions: {
    selectedTrain: null
  },
  dictionaries: {
    train: undefined
  },
  trainingId: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case START_TRAINING:
      return {
        ...state,
        trainingId: action.payload
      };

    case STOP_TRAINING: {
      return {
        ...state,
        trainingId: undefined
      };
    }

    case RECIEVE_TRAINING_ACTIONS:
      return {
        recorder: {
          record: false,
          stoped: true,
          pause: false,
          swaps: []
        },
        trainingId: undefined,
        middleBtnName: "radio-button-off",
        leftBtnName: "medical",
        rigthBtnName: "swap",
        trainOptions: {
          selectedTrain: null
        },
        dictionaries: {
          train: action.payload
        }
      };

    case TOGGLE_PLAY:
      const recorder = action.payload.recorder;
      const newRecorder = {
        ...state.recorder,
        record: !recorder.record,
        stoped: false,
        pause: !!recorder.record
      };

      const newState = {
        ...state,
        recorder: newRecorder,
        middleBtnName:
          recorder.stoped && recorder.record
            ? "radio-button-off"
            : recorder.record
              ? "pause"
              : "radio-button-on",
        leftBtnName: newRecorder.stoped ? "medical" : "md-add"
      };

      return newState;
    case STOP:
      return {
        ...state,
        recorder: {
          record: false,
          stoped: true,
          pause: false,
          swaps: []
        },
        middleBtnName: "radio-button-off",
        leftBtnName: "medical",
        trainOptions: {
          selectedTrain: null
        }
      };
    case SWAP_DIRECTION:
      let newSwaps = state.recorder.swaps;
      newSwaps.push({ time: new Date().getTime() });
      return {
        ...state,
        recorder: {
          ...state.recorder,
          swaps: newSwaps
        }
      };
    case SELECT_TRAIN:
      return {
        ...state,
        trainOptions: {
          selectedTrain: action.payload
        }
      };
    default:
      return state;
  }
}
