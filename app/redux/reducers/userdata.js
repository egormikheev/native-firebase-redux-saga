import Utils from "../../utils";
import l from "lodash";

import {
  RECIEVE_USER_ACTIVITY,
  ADD_TRAIN_SUCCESS,
  TOGGLE_TRAIN_TO_FAVORITES,
  ADD_PROGRAM_SUCCESS,
  TOGGLE_PROGRAM_TO_FAVORITES
} from "../actions/userdata";


import {
  START_PROGRAM,
  STOP_PROGRAM,
  PAUSE_PROGRAM,
  STOP
} from "../actions/training";

import {
  SEND_ACTIVITY
} from "../actions/timer";

import {
  INITIAL_LOADING_SUCCESS
} from "../actions/initLoad";

let initialState = {
  actions: null,
  trainings: null,
  programs: null,
  dictionaries: null,
  statuses: {
    initLoad: false,
    unfinishedProgram: false
  },
  favorites: {
    trainings: [],
    programs: []
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case RECIEVE_USER_ACTIVITY:
      return {
        actions: action.payload
      };
    case INITIAL_LOADING_SUCCESS:
      return {
        ...state,
        ...action.payload,
        statuses: { ...state.statuses,
          initLoad: true
        }
      };
    case TOGGLE_TRAIN_TO_FAVORITES:
      let newFavorites = l.clone(
        state.favorites.trainings.length > 0 ? state.favorites.trainings : []
      );

      if (action.payload.isFavorite) {
        newFavorites.push(action.payload.id);
      } else {
        newFavorites = newFavorites.filter(s => s != action.payload.id);
      }

      return {
        ...state,
        favorites: {
          ...state.favorites,
          trainings: newFavorites
        }
      };
    case TOGGLE_PROGRAM_TO_FAVORITES:
      let newFavoritesPr = l.clone(
        state.favorites.programs.length > 0 ? state.favorites.programs : []
      );

      if (action.payload.isFavorite) {
        newFavoritesPr.push(action.payload.id);
      } else {
        newFavorites = newFavoritesPr.filter(s => s != action.payload.id);
      }

      return {
        ...state,
        favorites: {
          ...state.favorites,
          programs: newFavoritesPr
        }
      };
    case ADD_TRAIN_SUCCESS:
      let train = action.payload;
      let newTrainings = state.trainings;
      let favorites = state.favorites.trainings;
      newTrainings.push(train);
      favorites.push(train.id);
      return {
        ...state,
        trainings: newTrainings,
        favorites: {
          ...state.favorites,
          trainings: favorites
        }
      };
    case ADD_PROGRAM_SUCCESS:
      let program = action.payload;
      let newPrograms = state.programs;
      let favoritesPr = state.favorites.programs;
      newPrograms.push(program);
      favoritesPr.push(program.id);
      return {
        ...state,
        programs: newPrograms,
        favorites: {
          ...state.favorites,
          programs: favoritesPr
        }
      };
    case START_PROGRAM:
      if (!action.payload.isContinue) {
        return {
          ...state,
          statuses: {
            ...state.statuses,
            unfinishedProgram: {
              uuid: action.payload.uuid,
              progress: {
                programID: action.payload.id,
                currentBlock: -1,
                currentTrain: -1
              }
            }
          }
        };
      }
      return state;

    case PAUSE_PROGRAM:
      {
        return {
          ...state,
          statuses: {
            ...state.statuses,
            unfinishedProgram: {
              ...action.payload
            }
          }
        };
      }

    case STOP_PROGRAM:
      return {
        ...state,
        statuses: {
          ...state.statuses,
          unfinishedProgram: false
        }
      };

    case SEND_ACTIVITY:
      let unfinishedProgram = state.statuses.unfinishedProgram;

      if (action.payload.activity.block) {
        unfinishedProgram.currentBlock = action.payload.activity.block;
      }

      if (action.payload.activity.train) {
        unfinishedProgram.currentTrain = action.payload.activity.train;
      }
      return {
        ...state,
        statuses: {
          ...state.statuses,
          unfinishedProgram
        }
      };
    default:
      return state;
  }
}
