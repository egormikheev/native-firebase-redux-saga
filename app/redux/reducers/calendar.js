import {
  GET_CALENDAR_DAYS_REQUEST,
  GET_CALENDAR_DAYS_SUCCESS
} from '../actions/calendar'

let initialState = {
  items: undefined,
  rawItems: undefined,
  selectDay: {}
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case GET_CALENDAR_DAYS_REQUEST:
      return {
        ...state,
        selectDay: action.payload
      }
    case GET_CALENDAR_DAYS_SUCCESS:
      return {
        ...state,
        rawItems: action.payload.days,
        mode: action.payload.mode
      }
    default:
      return state
  }
}
