import Utils from "../../utils";

import {
  GET_MORE_TRAININGS,
  APPLY_FILTER,
  APPLY_FILTER_PROGRAMS,
  getAllSensorData,
  clearAllSensorData
} from "../actions/userdata";

import { SET_PERIPHERALS } from "../actions/bluetooth";

import {
  INITIAL_LOADING_REQUEST,
  UPDATE_USER_INFO_SUCCESS
} from "../actions/initLoad";

import { REHYDRATE } from "redux-persist/constants";
import firebase from "react-native-firebase";

let initialState = {
  trainingsList: {
    page: 1,
    pageSize: 10000
  },
  programsList: {
    page: 1,
    pageSize: 10000
  },
  rehydrated: 0,
  action: false,
  filter: {
    values: {},
    isFilter: false
  },
  filterProgram: {
    values: {},
    isFilter: false
  },
  peripherals: new Map(),
  settings: {
    downloadCSV: {
      pending: false
    },
    clearData: {
      pending: false
    }
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_PERIPHERALS: {
      return {
        ...state,
        peripherals: action.payload
      };
    }
    case GET_MORE_TRAININGS:
      const page = ++state.trainingsList.page;
      return {
        ...state,
        trainingsList: {
          ...state.trainingsList,
          page
        }
      };
    case REHYDRATE:
      return {
        ...state,
        rehydrated: ++state.rehydrated
      };

    case APPLY_FILTER:
      return {
        ...state,
        filter: { values: action.payload, isFilter: !state.filter.isFilter }
      };

    case APPLY_FILTER_PROGRAMS:
      return {
        ...state,
        filterProgram: {
          values: action.payload,
          isFilter: !state.filter.isFilter
        }
      };

    case UPDATE_USER_INFO_SUCCESS:
      return {
        ...state,
        user: action.payload
      };
      case getAllSensorData.REQUEST:
      return {
        ...state,
        settings: {
          ...state.settings,
          downloadCSV: {
            pending: true
          }
        }
      }
    case getAllSensorData.SUCCESS:
    case getAllSensorData.ERROR:
      return {
        ...state,
        settings: {
          ...state.settings,
          downloadCSV: {
            pending: false
          }
        }
      }
    case clearAllSensorData.REQUEST:
      return {
        ...state,
        settings: {
          ...state.settings,
          clearData: {
            pending: true
          }
        }
      }
    case clearAllSensorData.SUCCESS:
    case clearAllSensorData.ERROR:
      return {
        ...state,
        settings: {
          ...state.settings,
          clearData: {
            pending: false
          }
        }
      }
    default:
      return state;
  }
}
