import BackgroundTimer from 'react-native-background-timer'

import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  takeLatest,
  all
} from 'redux-saga/effects'
import R from 'reactotron-react-native'
import l from 'lodash'
import m from 'moment'

import {
  getCalendarDays,
  getCalendarDaysSuccess,
  GET_CALENDAR_DAYS_REQUEST,
  resetCalendarDays
} from '../actions/calendar'

import { api } from '../../services'
import Utils from '../../utils'

import persist from '@redux-offline/redux-offline/lib/defaults/persist'

export function * getCalendarDaysFrom () {
  const getSelectedDay = state =>
    (state.calendar &&
      state.calendar.selectDay &&
      state.calendar.selectDay.dateString
      ? state.calendar.selectDay.dateString
      : m().format('YYYY-MM-DD'))

  const day = yield select(getSelectedDay)
  const days = yield call(api.getCalendarDaysActivity, day)

  const oldItems = state => state.calendar.rawItems
  const items = yield select(oldItems)

  // TODO: надо подумать грузить сразу весь календарь или мержить
  // const newDays = _.merge(items, days.val())

  yield put(getCalendarDaysSuccess(days.val(), 'add'))
}

export function * getCalendarDaysWatcher () {
  yield takeLatest(GET_CALENDAR_DAYS_REQUEST, getCalendarDaysFrom)
}
