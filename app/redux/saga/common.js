import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  takeLatest,
  all
} from 'redux-saga/effects'

import R from 'reactotron-react-native'
import l from 'lodash'
import m from 'moment'

import { getAllSensorData, clearAllSensorData } from '../actions/userdata'

import { api } from '../../services'
import RNFS from 'react-native-fs'
import { PermissionsAndroid } from 'react-native'

import Utils from '../../utils'

export function * getSensorData () {
  yield put(getAllSensorData.request())

  const data = yield call(api.getAllSensorData)

  let download = new Map()
  data.forEach(childSnapshot => {
    download.set(childSnapshot.key, Object.values(childSnapshot.val()))
  })

  const path =
    RNFS.ExternalStorageDirectoryPath +
    `/Documents/${m().format('YYYYMMDD')}data.txt`
  const filedata = JSON.stringify([...download], null, 2)

  Utils.log({ path, length: filedata.length })

  const [writePerm, readPerm] = yield all([
    call(
      PermissionsAndroid.request,
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      { title: 'Ok', message: 'write accepted' }
    ),
    call(
      PermissionsAndroid.request,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      { title: 'Ok', message: 'read accepted' }
    )
  ])

  if (writePerm && readPerm) {
    const writeResult = yield call(RNFS.writeFile, path, filedata, 'utf8')
    Utils.log({ writeResult })
    if (!writeResult) yield put(getAllSensorData.success())
    else yield put(getAllSensorData.error())
  } else {
    yield put(getAllSensorData.error())
  }
}

export function * getSensorDataWatcher () {
  yield takeLatest(getAllSensorData.TRIGGER, getSensorData)
}

export function * clearSensorData () {
  yield put(clearAllSensorData.request())

  const result = yield call(api.clearAllSensorData)
  yield put(clearAllSensorData.success())
}

export function * removeSensorDataWatcher () {
  yield takeLatest(clearAllSensorData.TRIGGER, clearSensorData)
}
