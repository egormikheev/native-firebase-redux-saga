import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  takeLatest,
  all
} from "redux-saga/effects";

import {
  initLoadingSuccess,
  initLoadingFailure,
  INITIAL_LOADING_REQUEST
} from "../actions/initLoad";

import { api } from "../../services";
import Utils from "../../utils";
import l from "lodash";

export function* initLoad(action) {
  const getLoadStatus = state => state.userdata;
  const userdata = yield select(getLoadStatus);
  if (userdata.statuses.initLoad) {
    initLoadingSuccess(userdata);
  }
  /*
  - Справочник упражнений и программ пользователя и системы
  - Данные пользователя
  - История пользовательского календаря - вся
  */

  let user = action.payload && action.payload.uid ? action.payload.uid : false;

  let [
    trainings,
    programs,
    equipment,
    level,
    muscleGroup,
    type,
    typeTrain
  ] = yield all([
    call(api.getTrainings, user, true),
    call(api.getPrograms, user, true),
    call(api.getDictionary, "dictionaries/train/equipment"),
    call(api.getDictionary, "dictionaries/train/level"),
    call(api.getDictionary, "dictionaries/train/muscleGroup"),
    call(api.getDictionary, "dictionaries/train/type"),
    call(api.getDictionary, "dictionaries/train/typeTrain")
  ]);

  //доп вычисления
  programs = programs.map(p => {
    return {
      ...p,
      programsMap: p.programs.map(b => (b.trains ? b.trains.length : 0)),
      countPrograms: p.programs.length
    };
  });

  const data = {
    trainings,
    programs,
    dictionaries: {
      equipment,
      level,
      muscleGroup,
      type,
      typeTrain,
      programClass: l.uniqBy(
        programs.map(s => {
          return { name: s.class };
        }),
        "name"
      )
    }
  };

  yield put(
    initLoadingSuccess({
      ...data,
      trainings: data.trainings.sort((x, y) =>
        x.muscleGroup.localeCompare(y.muscleGroup)
      ),
      programs: data.programs.sort((x, y) => x.class.localeCompare(y.class))
    })
  );
}

export function* initLoadWatcher(action) {
  yield takeLatest(INITIAL_LOADING_REQUEST, initLoad);
}
