/* eslint-disable no-constant-condition */

import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  all
} from 'redux-saga/effects'

import {
  getDictionaries,
  startRecordWatcher,
  stopRecorderWatcher,
  addToCalendarWatcher
} from './training.js'

import { getSensorDataWatcher, removeSensorDataWatcher } from './common'

import { getCalendarDaysWatcher, getInitCalendarDays } from './calendar'
import { addNewTrainWatcher } from './program'

import { initLoadWatcher } from './initLoad'

export default function * root () {
  yield all([
    fork(addNewTrainWatcher),
    fork(initLoadWatcher),
    fork(startRecordWatcher),
    fork(stopRecorderWatcher),
    fork(getCalendarDaysWatcher),
    fork(addToCalendarWatcher),
    fork(getSensorDataWatcher),
    fork(removeSensorDataWatcher)
  ])
}
