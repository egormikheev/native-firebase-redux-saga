import BackgroundTimer from 'react-native-background-timer'

import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  all
} from 'redux-saga/effects'

import R from 'reactotron-react-native'
import m from 'moment'

import {
  recieveTrainingActions,
  RECIEVE_TRAINING_ACTIONS
} from '../actions/dictionaries'

import { getCalendarDays, getCalendarDaysSuccess } from '../actions/calendar'

import { TOGGLE_PLAY, STOP, SWAP_DIRECTION } from '../actions/training'

import { SEND_ACTIVITY } from '../actions/timer'

import {
  START_TIMER,
  STOP_TIMER,
  RESET_TIMER,
  startTimer,
  stopTimer,
  resetTimer
} from '../actions/timer'

import { api } from '../../services'
import Utils from '../../utils'

export function * getDictionaries () {
  const trainings = yield call(api.getDictionaries)
  yield put(recieveTrainingActions(Utils.snapshotToArray(trainings)))
}

let flag = false

export function * togglePlay () {
  const getTrainingState = state => state.training
  const training = yield select(getTrainingState)
  if (training.recorder.record && !training.recorder.stop && !flag) {
    flag = true

    yield put(startTimer())
  } else if (
    !training.recorder.record &&
    !training.recorder.stop &&
    flag === true
  ) {
    yield put(stopTimer())
    flag = false
  }
}

export function * stop () {
  yield put(resetTimer())
  flag = false
}

export function * stopRecorderWatcher () {
  yield takeEvery(STOP, stop)
}

export function * startRecordWatcher () {
  yield takeEvery(TOGGLE_PLAY, togglePlay)
}

export function * addToCalendar (action) {
  const oldItems = state => state.calendar.rawItems
  const items = yield select(oldItems)
  let newItems

  if (items) {
    items[`${action.payload.dateStop}`] = {
      ...action.payload,
      formatDate: m(action.payload.dateStop).format('YYYY-MM-DD')
    }

    newItems = items
  } else {
    newItems = {
      [action.payload.dateStop]: {
        ...action.payload,
        formatDate: m(action.payload.dateStop).format('YYYY-MM-DD')
      }
    }
  }

  yield put(getCalendarDaysSuccess(items, 'add'))
}

export function * addToCalendarWatcher () {
  yield takeEvery(SEND_ACTIVITY, addToCalendar)
}
