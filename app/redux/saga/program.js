import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  takeLatest,
  all
} from "redux-saga/effects";

import R from "reactotron-react-native";
import l from "lodash";
import m from "moment";
import firebase from "react-native-firebase";

import { ADD_TRAIN_REQUEST, ADD_TRAIN_SUCCESS } from "../actions/userdata";

import { api } from "../../services";

import Utils from "../../utils";

export function* addTrain(action) {
  var user = firebase.auth().currentUser;
  let train = action.payload;
  train.userId = user.uid ? user.uid : "0000000000000000000000000001";
  const id = yield call(api.addNewTrain, train);
  train.id = id;
  yield put({ type: ADD_TRAIN_SUCCESS, payload: train });
}

export function* addNewTrainWatcher() {
  yield takeLatest(ADD_TRAIN_REQUEST, addTrain);
}
