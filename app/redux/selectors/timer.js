import { createSelector } from 'reselect'

const timerPointer = state => state.timer

export default createSelector([timerPointer], timer => {
  return {
    timer
  }
})
