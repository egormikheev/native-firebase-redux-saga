import { createSelector } from "reselect";
import { AsyncStorage } from "react-native";
// import storage from 'redux-persist/lib/storage'
import Utils from "../../utils";

const statePointer = state => state.session;
const initLoadStatusPointer = state => state.userdata.statuses.initLoad;

export default createSelector(
  [statePointer, initLoadStatusPointer],
  (state, initLoad) => {
    return { ...state, initLoad };
  }
);
