import { createSelector } from "reselect";
import Utils from "../../utils";

const userDataProgramPointer = state => state.userdata.programs;
const userDataFavoritesPointer = state => state.userdata.favorites;
const userDataDictionariesPointer = state => state.userdata.dictionaries;
const sessionFilterPointer = state => state.session.filterProgram;
const sessionProgrammListPointer = state => state.session.programsList;
const userDataTrainingsPointer = state => state.userdata.trainings;

const filterSelector = createSelector(
  [userDataProgramPointer, sessionFilterPointer],
  (programs, session) => {
    let action = false;

    let filterItems = [];

    if (programs && programs.length > 0) {
      if (
        session.values &&
        session.values.programClass &&
        session.values.programClass.index != -1
      ) {
        filterItems.push(1);
        programs = programs.filter(
          s => s.class == session.values.programClass.name
        );
      }

      return {
        programs,
        action: filterItems.length > 0 ? "setFilter" : "resetFilter"
      };
    }
  }
);

export default createSelector(
  [
    filterSelector,
    userDataFavoritesPointer,
    userDataDictionariesPointer,
    sessionProgrammListPointer,
    userDataTrainingsPointer
  ],
  (filter, favorites, dictionaries, programsList, trainings) => {
    let filteredPrograms = [];

    const page = programsList.page;
    const size = programsList.pageSize;
    const itemsCount = page * size;

    if (filter && filter.programs && filter.programs.length > 0) {
      filteredPrograms = filter.programs.slice(0, itemsCount).map(i => {
        if (
          favorites.programs.length > 0 &&
          favorites.programs.includes(i.id)
        ) {
          return {
            ...i,
            isFavorite: true
          };
        } else
          return {
            ...i,
            isFavorite: false
          };
      });
    }

    return {
      favorites,
      dictionaries,
      filteredPrograms,
      action: filter && filter.action ? filter.action : "",
      trainings
    };
  }
);
