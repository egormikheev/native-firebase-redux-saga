import { createSelector } from 'reselect'

const settingsPointer = state => state.session.settings

export default createSelector([settingsPointer], settings => {
  return {
    settings
  }
})