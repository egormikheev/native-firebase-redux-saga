import { createSelector } from "reselect";
import Utils from "../../utils";

const trainPointer = state => state.training;
const timersPointer = state => state.timers;

const userDataTrainingsPointer = state => state.userdata.trainings;
const userDataProgramsPointer = state => state.userdata.programs;
const userDataFavoritesPointer = state => state.userdata.favorites;
const userDataUnfinishedProgramPointer = state =>
  state.userdata.statuses.unfinishedProgram;

export default createSelector(
  [
    trainPointer,
    timersPointer,
    userDataTrainingsPointer,
    userDataProgramsPointer,
    userDataFavoritesPointer,
    userDataUnfinishedProgramPointer
  ],
  (training, timers, trainings, programs, favorites, program) => {
    let filteredPrograms = programs.filter(s =>
      favorites.programs.includes(s.id)
    );

    let extendedPrograms = filteredPrograms.map(st => {
      return {
        ...st,
        programs: st.programs.map(pr => {
          return {
            ...pr,
            trains: pr.trains
              ? pr.trains.map(t => {
                  let train = trainings.find(s => s.uri == t.trainRef);
                  return {
                    ...t,
                    ...train
                  };
                })
              : null
          };
        })
      };
    });

    Utils.log(extendedPrograms);

    return {
      training: {
        ...training,
        dictionaries: {
          train: trainings.filter(s => favorites.trainings.includes(s.id)),
          programs: extendedPrograms
        }
      },
      timers,
      statuses: {
        unfinishedProgram: program
      }
    };
  }
);
