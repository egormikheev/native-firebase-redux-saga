import { createSelector } from 'reselect'
import r from 'reactotron-react-native'
import m from 'moment'
import l from 'lodash'
import fp from 'lodash/fp'
import Utils from '../../utils'

const calendarPointer = state => state.calendar

export default createSelector([calendarPointer], calendar => {
  let itemsarr = []
  let items = []

  for (let item in calendar.rawItems) {
    itemsarr.push({
      ...calendar.rawItems[item],
      totalTime: calendar.rawItems[item]
        ? calendar.rawItems[item].ticks
            .map(i => i.duration)
            .reduce((s, i) => s + i)
        : 0,
      totalSwaps: calendar.rawItems[item]
        ? calendar.rawItems[item].ticks.map(i => i.swaps).length
        : 0
    })
  }

  items = l.groupBy(itemsarr, 'formatDate')

  // сгруппировать по тренировкам
  let groupedItems = {}

  for (let item in items) {
    let train = l.groupBy(items[item], 'trainingId')
    let keys = Object.keys(train)
    let trains = []
    for (let key of keys) {
      let dateStart = train[key].map(d => d.dateStart).sort((x, y) => x - y)[0]
      let dateStop = train[key].map(d => d.dateStop).sort((x, y) => y - x)[0]
      let duration = dateStop - dateStart
      let tr = {
        trainingId: key,
        timeStart: m(dateStart).format('LT'),
        timeStop: m(dateStop).format('LT'),
        dateStart,
        dateStop,
        trainingDuration: duration,
        formatDuration: m.utc(duration).format('HH:mm:ss'),
        actions: train[key],
        countActions: train[key].length
      }
      trains.push(tr)
    }
    groupedItems[item] = trains.sort((x, y) => x.dateStart - y.dateStart)
  }

  for (let i = -25; i < 45; i++) {
    const time = m
      .unix(calendar.selectDay.timestamp / 1000)
      .day(i)
      .format('YYYY-MM-DD')
    if (!groupedItems[time]) {
      groupedItems[time] = []
    }
  }

  return {
    ...calendar,
    items: groupedItems
  }
})
