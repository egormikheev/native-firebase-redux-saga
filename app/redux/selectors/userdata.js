import { createSelector } from "reselect";
import Utils from "../../utils";

const userDataTrainingsPointer = state => state.userdata.trainings;
const userDataFavoritesPointer = state => state.userdata.favorites;
const userDataDictionariesPointer = state => state.userdata.dictionaries;
const sessionFilterPointer = state => state.session.filter;
const sessionTrainingListPointer = state => state.session.trainingsList;

const filterSelector = createSelector(
  [userDataTrainingsPointer, sessionFilterPointer],
  (trainings, session) => {
    let action = false;

    let filterItems = [];

    if (trainings && trainings.length > 0) {
      if (
        session.values &&
        session.values.muscleGroup &&
        session.values.muscleGroup.index != -1
      ) {
        filterItems.push(1);
        trainings = trainings.filter(
          s => s.muscleGroup == session.values.muscleGroup.name
        );
      }

      if (
        session.values &&
        session.values.equipment &&
        session.values.equipment.index != -1
      ) {
        filterItems.push(1);
        trainings = trainings.filter(
          s => s.equipment == session.values.equipment.name
        );
      }

      if (
        session.values &&
        session.values.typeTrain &&
        session.values.typeTrain.index != -1
      ) {
        filterItems.push(1);
        trainings = trainings.filter(
          s => s.typeTrain == session.values.typeTrain.name
        );
      }

      return {
        trainings,
        action: filterItems.length > 0 ? "setFilter" : "resetFilter"
      };
    }
  }
);

export default createSelector(
  [
    filterSelector,
    userDataFavoritesPointer,
    userDataDictionariesPointer,
    sessionTrainingListPointer
  ],
  (filter, favorites, dictionaries, traininggList) => {
    let filteredTrainings = [];

    const page = traininggList.page;
    const size = traininggList.pageSize;
    const itemsCount = page * size;

    if (filter && filter.trainings && filter.trainings.length > 0) {
      filteredTrainings = filter.trainings.slice(0, itemsCount).map(i => {
        if (
          favorites.trainings.length > 0 &&
          favorites.trainings.includes(i.id)
        ) {
          return {
            ...i,
            isFavorite: true,
            steps: Array.isArray(i.steps) ? i.steps : [i.steps]
          };
        } else
          return {
            ...i,
            isFavorite: false,
            steps: Array.isArray(i.steps) ? i.steps : [i.steps]
          };
      });
    }

    return {
      userdata: {
        favorites,
        dictionaries,
        filteredTrainings,
        action: filter && filter.action ? filter.action : ""
      }
    };
  }
);
