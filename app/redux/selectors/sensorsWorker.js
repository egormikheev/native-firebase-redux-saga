import { createSelector } from "reselect";

const peripheralsPointer = state => state.session.peripherals;

export default createSelector([peripheralsPointer], peripherals => {
  return { peripherals };
});
