import { AppRegistry, YellowBox } from 'react-native'
import { Root } from 'native-base'
import App from './App'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { offline, createOffline } from '@redux-offline/redux-offline'
import offlineConfig from '@redux-offline/redux-offline/lib/defaults'

import FilesystemStorage from 'redux-persist-filesystem-storage'
import { persistStore, autoRehydrate } from 'redux-persist'

import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './app/redux/reducers'
import rootSaga from './app/redux/saga'
import { rehydrateStore } from './app/redux/actions/session'
import Utils from './app/utils'

let Reactotron
if (__DEV__) {
  require('./ReactotronConfig')
  Reactotron = require('reactotron-react-native').default
}

let sagaMiddleware
if (__DEV__) {
  sagaMiddleware = createSagaMiddleware(Reactotron.createSagaMonitor())
} else sagaMiddleware = createSagaMiddleware()

const middlewareList = [sagaMiddleware]

const {
  middleware: offlineMiddleware,
  enhanceReducer,
  enhanceStore
} = createOffline({
  ...offlineConfig,

  persistCallback: () => {
    rehydrateStore()
  },
  persistOptions: {
    blacklist: ['calendar', 'session', 'training', 'timers'],
    storage: FilesystemStorage
  }
})

const middleware = applyMiddleware(...middlewareList, offlineMiddleware)

let store
if (__DEV__) {
  store = Reactotron.createStore(
    enhanceReducer(rootReducer),
    compose(middleware, enhanceStore)
  )
} else {
  store = createStore(
    enhanceReducer(rootReducer),
    compose(middleware, enhanceStore)
  )
}

sagaMiddleware.run(rootSaga)

class ReduxApp extends Component {
  render () {
    return (
      <Provider store={store}>
        <Root>
          <App />
        </Root>
      </Provider>
    )
  }
}

AppRegistry.registerComponent('bigdog', () => ReduxApp)
