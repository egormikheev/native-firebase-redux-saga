# React Native template

### Описание архитектуры приложения

1. Хранение данных в редакс хранилище с возможностью сохранения состояния приложения в постоянном хранилище посредством библиотеки redux-offline
2. Испрользование библиотеки saga
3. Использование библиотеки reactotron для процесса отладки приложения

### Дополнительные библиотеки

1. Firestore
2. Ble manager
3. React navigation
4. и др

## Порядок настройки окружения

### Переменные окружения

- ANDROID_HOME : C:\Users\User\AppData\Local\Android\Sdk
- REACT_NATIVE_PACKAGER_HOSTNAME : IP вашего ПК в сети за NAT
- PATH: ['C:\Program Files\Java\jdk1.8.0_121\bin', 'C:\Users\User\AppData\Local\Android\Sdk\tools', 'C:\Users\User\AppData\Local\Android\Sdk\platform-tools']
- add system variable: JAVA_HOME --> C:\Program Files\Java\jre1.8.0_xxx

### В приложении

- ReactotronConfig.js => указать IP ПК за NAT
