import React from 'react'
import {
  createSwitchNavigator,
  createDrawerNavigator,
  createAppContainer
} from 'react-navigation'

// import the different screens
import Loading from './app/containers/auth/loading'
import SignUp from './app/containers/auth/signup'
import Login from './app/containers/auth/login'

// login with phone auth (can't work with fucking tele2)
// import Login from "./app/containers/auth/authphone";

import ProgramsNavigator from './app/routing/programs.js'
import TrainNavigator from './app/routing/train'
import ProfileNavigator from './app/routing/profile'
import SW from './app/services/sensorsWorker'

const Authorized = createDrawerNavigator(
  {
    Profile: { screen: ProfileNavigator },
    Program: { screen: ProgramsNavigator },
    Train: { screen: TrainNavigator }
  },
  {
    contentComponent: props => <SW {...props} />,
    initialRouteName: 'Profile'
  }
)

// create our app's navigation stack
const rootStack = createSwitchNavigator(
  {
    Loading,
    SignUp,
    Login,
    Authorized
  },
  {
    initialRouteName: 'Loading'
  }
)

export default createAppContainer(rootStack)
